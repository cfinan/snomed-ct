"""ORM classes for the prescription-string database.
"""
from snomed_ct import common
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    BigInteger,
    SmallInteger,
    ForeignKey,
    Date,
    Boolean,
    UniqueConstraint,
    Text,
    Index
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# DirInfo(path='/data/snomedct/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z', modules=[], edition='International', release=['Snapshot', 'Full'], version='PRODUCTION', date=datetime.datetime(2023, 1, 31, 0, 0), dir_date=datetime.datetime(2023, 1, 31, 0, 0), delta_from_date=None, delta_to_date=None, lang_refsets=[900000000000508004, 900000000000509007])
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Version(Base):
    """A representation of the ``version`` table.

    Parameters
    ----------
    version_id : `int`
        An auto-incremented primary key.
    version_date : `date.date`
        The date extracted from the release info JSON file.
    delta_from_date : `date.date`, optional, default: `NoneType`
        The delta from date extracted from the release info JSON file.
    delta_to_date : `date.date`, optional, default: `NoneType`
        The delta to date extracted from the release info JSON file.
    edition : `str`
        The SnomedCT edition that has been built (length 50).
    release : `str`
        The SnomedCT release that has been built (length 50).
    version_type : `str`
        The version type that has been built (length 50).
    dir_basename : `str`
        The basename of the root directory of the extracted zip archive (length
        255).
    version_module : `snomed_ct.orm.VersionModule`, optional, default: `NoneType`
        The relationship link back to the version modules table.
    version_lang : `snomed_ct.orm.VersionLanguage`, optional, default: `NoneType`
        The relationship link back to the version languages table.

    Notes
    -----
    The version table is not actually in the SnomedCT database schema, but I
    have added it for reference, it is simply the data in the release JSON
    metadata and the root directory name.
    """
    __tablename__ = 'version'

    version_id = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key."
    )
    version_date = Column(
        Date, nullable=False,
        doc="The date extracted from the release info JSON file."
    )
    delta_from_date = Column(
        Date, nullable=True,
        doc="The delta from date extracted from the release info JSON file."
    )
    delta_to_date = Column(
        Date, nullable=True,
        doc="The delta to date extracted from the release info JSON file."
    )
    edition = Column(
        String(50), nullable=False,
        doc="The SnomedCT edition that has been built."
    )
    release = Column(
        String(50), nullable=False,
        doc="The SnomedCT release that has been built."
    )
    version_type = Column(
        String(50), nullable=False,
        doc="The version type that has been built."
    )
    dir_basename = Column(
        String(255), nullable=False,
        doc="The basename of the root directory of the extracted zip archive."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    modules = relationship(
        'VersionModule',
        back_populates='version',
        doc="The relationship link back to the version modules table."
    )
    languages = relationship(
        'VersionLanguage',
        back_populates='version',
        doc="The relationship link back to the version languages table."
    )
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VersionModule(Base):
    """A representation of the ``version_module`` table.

    Parameters
    ----------
    version_module_id : `int`
        An auto-incremented primary key.
    version_id : `int`
        A link from the version language to the version. Foreign key to
        ``version.version_id``.
    concept_id : `int`
        The SnomedCT concept ID for the version language. Foreign key to
        ``sctid_concept.concept_id``.
    version : `snomed_ct.orm.Version`, optional, default: `NoneType`
        The relationship link back to the main version table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Version module Concept ID relationship link to the concept lookup
        table.

    Notes
    -----
    The version_module table is not actually in the SnomedCT database schema,
    but I have added it for reference, it is simply the data in the release
    JSON metadata.
    """
    __tablename__ = 'version_module'

    version_module_id = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key."
    )
    version_id = Column(
        Integer, ForeignKey('version.version_id'), nullable=False,
        doc="A link from the version language to the version."
    )
    concept_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'), nullable=False,
        doc="The SnomedCT concept ID for the version language."
    )
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    version = relationship(
        'Version',
        foreign_keys=[version_id],
        back_populates='modules',
        doc="The relationship link back to the main version table."
    )
    concept = relationship(
        'ConceptId',
        foreign_keys=[concept_id],
        back_populates='version_module',
        doc="Version module Concept ID relationship link to the concept "
        "lookup table."
    )
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VersionLanguage(Base):
    """A representation of the ``version_lang`` table.

    Parameters
    ----------
    version_lang_id : `int`
        An auto-incremented primary key.
    version_id : `int`
        A link from the version language to the version. Foreign key to
        ``version.version_id``.
    concept_id : `int`
        The SnomedCT concept ID for the version language . Foreign key to
        ``sctid_concept.concept_id``.
    version : `snomed_ct.orm.Version`, optional, default: `NoneType`
        The relationship link back to the main version table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Version language Concept ID relationship link to the concept lookup
        table.

    Notes
    -----
    The version_lang table is not actually in the SnomedCT database schema,
    but I have added it for reference, it is simply the data in the release
    JSON metadata.
    """
    __tablename__ = 'version_lang'

    version_lang_id = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key."
    )
    version_id = Column(
        Integer, ForeignKey('version.version_id'), nullable=False,
        doc="A link from the version language to the version."
    )
    concept_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'), nullable=False,
        doc="The SnomedCT concept ID for the version language ."
    )
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    version = relationship(
        'Version',
        foreign_keys=[version_id],
        back_populates='languages',
        doc="The relationship link back to the main version table."
    )
    concept = relationship(
        'ConceptId',
        foreign_keys=[concept_id],
        back_populates='version_lang',
        doc="Version language Concept ID relationship link to the concept "
        "lookup table."
    )
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctConcept(Base):
    """A representation of the snomed clinical concepts table.

    Parameters
    ----------
    concept_pk : `int`
        An auto-incremented primary key.
    concept_id : `int`
        Link back to the concept lookup table. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    effective_time : `date.date`
        The date for the concept.
    is_active : `bool`
        Is the concept active.
    module_id : `int`
        The module ID for the concept. Module IDs are the sourcecomponents for
        the concepts, i.e. is it from SNOMED CT core, SNOMED CT UK clinical
        extension module or SNOMED CT UK drug extension module. There seems to
        be 8 unique modules in this table. I think there are 9 overall, these
        can be seen the the ``release_package_information.json`` file within
        the source zip archive. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    definition_status_id : `int`
        The definition status for the concept. There seems to be only 2 unique
        entries in here, that link with descriptions. These are ``Defined`` and
        ``Necessary but not sufficient concept definition status``. This is
        indexed. Foreign key to ``sctid_concept.concept_id``.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Module ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Definition statust ID relationship link to the concept lookup table.

    Notes
    -----
    This is different from the concept ID lookup table and is only guaranteed
    to be unique across ``concept_id``, ``effective_time``, ``module_id``.
    """
    __tablename__ = 'sct2_concept'

    concept_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key."
    )
    concept_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="Link back to the concept lookup table."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the concept."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the concept active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the concept. Module IDs are the source"
        "components for the concepts, i.e. is it from SNOMED CT core, "
        "SNOMED CT UK clinical extension module or SNOMED CT UK drug extension"
        " module. There seems to be 8 unique modules in this table. I think"
        " there are 9 overall, these can be seen the the "
        "``release_package_information.json`` file within the source zip "
        "archive."
    )
    definition_status_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The definition status for the concept. There seems to be only 2 "
        "unique entries in here, that link with descriptions. These are "
        "``Defined`` and ``Necessary but not sufficient concept definition "
        "status``."
    )

    # -------------------------------------------------------------------------~
    concept = relationship(
        'ConceptId',
        foreign_keys=[concept_id],
        back_populates='concept',
        doc="Concept ID relationship link to the concept lookup table."
    )
    module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='concept_module',
        doc="Module ID relationship link to the concept lookup table."
    )
    definition_status = relationship(
        'ConceptId',
        foreign_keys=[definition_status_id],
        back_populates='concept_definition_status',
        doc="Definition statust ID relationship link to the concept lookup"
        " table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'concept_id',
                'effective_time',
                'module_id',
                name='concept_status_uc'
            ),
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctDescription(Base):
    """A representation of the snomed clinical concept text descriptions table.

    Parameters
    ----------
    desc_pk : `int`
        All through auto-increment, these are actually imported.
    description_id : `int`
        Link back to the descriptions lookup table. This is indexed. Foreign
        key to ``sctid_description.description_id``.
    effective_time : `date.date`
        The date for the description.
    is_active : `bool`
        Is the description active.
    module_id : `int`
        The module ID for the concept. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    concept_id : `int`
        The snomed concept ID that maps to the description. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    language_code : `str`
        The language code for the description (length 2).
    type_id : `int`
        The type mapping for the description. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    term : `str`
        The actual description text. This is indexed (length text).
    case_significance_id : `int`
        The case significance identifier for the description. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    sctid_description : `snomed_ct.orm.DescriptionId`, optional, default: `NoneType`
        Description ID relationship link to the description lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Module ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Type ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Case significance ID relationship link to the concept lookup table.

    Notes
    -----
    This is different from the description_id lookup table and is only
    guaranteed to be unique across ``description_id``, ``effective_time``,
    ``module_id``.
    """
    __tablename__ = 'sct2_description'

    desc_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="All through auto-increment, these are actually imported"
    )
    description_id = Column(
        BigInteger, ForeignKey('sctid_description.description_id'),
        nullable=False, index=True,
        doc="Link back to the descriptions lookup table."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the description."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the description active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the concept."
    )
    concept_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the description."
    )
    language_code = Column(
        String(2), nullable=False,
        doc="The language code for the description."
    )
    type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The type mapping for the description."
    )
    term = Column(
        Text, nullable=False, index=True,
        doc="The actual description text."
    )
    case_significance_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The case significance identifier for the description."
    )

    # -------------------------------------------------------------------------~
    description = relationship(
        'DescriptionId',
        foreign_keys=[description_id],
        back_populates='description',
        doc="Description ID relationship link to the description lookup table."
    )
    concept = relationship(
        'ConceptId',
        foreign_keys=[concept_id],
        back_populates='description',
        doc="Concept ID relationship link to the concept lookup table."
    )
    desc_module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='desc_module',
        doc="Module ID relationship link to the concept lookup table."
    )
    desc_type = relationship(
        'ConceptId',
        foreign_keys=[type_id],
        back_populates='desc_type',
        doc="Type ID relationship link to the concept lookup table."
    )
    desc_case_significance = relationship(
        'ConceptId',
        foreign_keys=[case_significance_id],
        back_populates='desc_case_significance',
        doc="Case significance ID relationship link to the concept lookup "
        "table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'description_id',
                'effective_time',
                'module_id',
                name='desc_status_uc'
            ),
            # Index('term_idx', term, mysql_length=255, mariadb_length=255),
            Index(
                'term_idx', term,
                mysql_prefix='FULLTEXT',
                # mariadb_prefix='FULLTEXT'
            )
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctRelationship(Base):
    """A representation of the snomed clinical concept relationships table.

    Parameters
    ----------
    rel_pk : `int`
        Auto-incremented primary key.
    relationship_id : `int`
        Link back to the relationships lookup table. This is indexed. Foreign
        key to ``sctid_relationship.relationship_id``.
    effective_time : `date.date`
        The date for the relationship.
    is_active : `bool`
        Is the relationship active.
    module_id : `int`
        The module ID for the relationship. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    source_id : `int`
        The snomed concept ID that maps to the source relationship. This is
        indexed. Foreign key to ``sctid_concept.concept_id``.
    destination_id : `int`
        The snomed concept ID that maps to the destination relationship. This
        is indexed. Foreign key to ``sctid_concept.concept_id``.
    relationship_group : `int`
        The relationship group identifier.
    type_id : `int`
        The concept identifier for the relationship type. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    characteristic_type_id : `int`
        The concept identifier for the relationship characteristic type. This
        is indexed. Foreign key to ``sctid_concept.concept_id``.
    modifier_id : `int`
        The concept identifier for the relationship modifier. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    sctid_relationship : `snomed_ct.orm.RelationshipId`, optional, default: `NoneType`
        Relationship ID relationship link to the relationships lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Module concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The source term concept ID relationship link to the concept lookup
        table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The destination concept ID relationship link to the concept lookup
        table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The type concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The characteristic type concept ID relationship link to the concept
        lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The modifier concept ID relationship link to the concept lookup table.

    Notes
    -----
    This is different from the relationship_id lookup table and is only
    guaranteed to be unique across ``relationship_id``, ``effective_time``,
    ``module_id``.
    """
    __tablename__ = 'sct2_relationship'

    rel_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="Auto-incremented primary key."
    )
    relationship_id = Column(
        BigInteger, ForeignKey('sctid_relationship.relationship_id'),
        nullable=False, index=True,
        doc="Link back to the relationships lookup table."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the relationship."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the relationship active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the relationship."
    )
    source_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the source relationship."
    )
    destination_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the destination relationship."
    )
    relationship_group = Column(
        Integer, nullable=False,
        doc="The relationship group identifier."
    )
    type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship type."
    )
    characteristic_type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship characteristic type."
    )
    modifier_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship modifier."
    )

    # -------------------------------------------------------------------------~
    rel = relationship(
        'RelationshipId',
        foreign_keys=[relationship_id],
        back_populates='rel',
        doc="Relationship ID relationship link to the relationships lookup"
        " table."
    )
    module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='rel_module',
        doc="Module concept ID relationship link to the concept lookup table."
    )
    source = relationship(
        'ConceptId',
        foreign_keys=[source_id],
        back_populates='rel_source',
        doc="The source term concept ID relationship link to the concept "
        "lookup table."
    )
    destination = relationship(
        'ConceptId',
        foreign_keys=[destination_id],
        back_populates='rel_destination',
        doc="The destination concept ID relationship link to the concept"
        " lookup table."
    )
    rel_type = relationship(
        'ConceptId',
        foreign_keys=[type_id],
        back_populates='rel_type',
        doc="The type concept ID relationship link to the concept lookup "
        "table."
    )
    char_type = relationship(
        'ConceptId',
        foreign_keys=[characteristic_type_id],
        back_populates='rel_char_type',
        doc="The characteristic type concept ID relationship link to the "
        "concept lookup table."
    )
    modifier = relationship(
        'ConceptId',
        foreign_keys=[modifier_id],
        back_populates='rel_modifier',
        doc="The modifier concept ID relationship link to the concept lookup "
        "table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'relationship_id',
                'effective_time',
                'module_id',
                name='desc_status_uc'
            ),
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctStatedRelationship(Base):
    """A representation of the snomed clinical concept stated relationships
    table.

    Parameters
    ----------
    rel_pk : `int`
        Auto-incremented primary key.
    relationship_id : `int`
        Link back to the relationships lookup table. This is indexed. Foreign
        key to ``sctid_relationship.relationship_id``.
    effective_time : `date.date`
        The date for the relationship.
    is_active : `bool`
        Is the relationship active.
    module_id : `int`
        The module ID for the relationship. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    source_id : `int`
        The snomed concept ID that maps to the source relationship. This is
        indexed. Foreign key to ``sctid_concept.concept_id``.
    destination_id : `int`
        The snomed concept ID that maps to the destination relationship. This
        is indexed. Foreign key to ``sctid_concept.concept_id``.
    relationship_group : `int`
        The relationship group identifier.
    type_id : `int`
        The concept identifier for the relationship type. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    characteristic_type_id : `int`
        The concept identifier for the relationship characteristic type. This
        is indexed. Foreign key to ``sctid_concept.concept_id``.
    modifier_id : `int`
        The concept identifier for the relationship modifier. This is indexed.
        Foreign key to ``sctid_concept.concept_id``.
    sctid_relationship : `snomed_ct.orm.RelationshipId`, optional, default: `NoneType`
        Relationship ID relationship link to the relationships lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        Module concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The source term concept ID relationship link to the concept lookup
        table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The destination concept ID relationship link to the concept lookup
        table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The type concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The characteristic type concept ID relationship link to the concept
        lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The modifier concept ID relationship link to the concept lookup table.

    Notes
    -----
    This is different from the relationship_id lookup table and is only
    guaranteed to be unique across ``relationship_id``, ``effective_time``,
    ``module_id``. I am not sure on what the difference is between this and the
    sct2_relationship table. Although, this table currently has no data.

    See also
    --------
    snomed_ct.orm.RelationshipId
    snomed_ct.orm.ConceptId
    """
    __tablename__ = 'sct2_stated_relationship'

    rel_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="Auto-incremented primary key."
    )
    relationship_id = Column(
        BigInteger, ForeignKey('sctid_relationship.relationship_id'),
        nullable=False, index=True,
        doc="Link back to the relationships lookup table."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the relationship."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the relationship active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the relationship."
    )
    source_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the source relationship."
    )
    destination_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the destination relationship."
    )
    relationship_group = Column(
        Integer, nullable=False,
        doc="The relationship group identifier."
    )
    type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship type."
    )
    characteristic_type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship characteristic type."
    )
    modifier_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The concept identifier for the relationship modifier."
    )

    # -------------------------------------------------------------------------~
    rel = relationship(
        'RelationshipId',
        foreign_keys=[relationship_id],
        back_populates='stated_rel',
        doc="Relationship ID relationship link to the relationships lookup"
        " table."
    )
    module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='stated_rel_module',
        doc="Module concept ID relationship link to the concept lookup table."
    )
    source = relationship(
        'ConceptId',
        foreign_keys=[source_id],
        back_populates='stated_rel_source',
        doc="The source term concept ID relationship link to the concept "
        "lookup table."
    )
    destination = relationship(
        'ConceptId',
        foreign_keys=[destination_id],
        back_populates='stated_rel_destination',
        doc="The destination concept ID relationship link to the concept"
        " lookup table."
    )
    stated_rel_type = relationship(
        'ConceptId',
        foreign_keys=[type_id],
        back_populates='stated_rel_type',
        doc="The type concept ID relationship link to the concept lookup "
        "table."
    )
    char_type = relationship(
        'ConceptId',
        foreign_keys=[characteristic_type_id],
        back_populates='stated_rel_char_type',
        doc="The characteristic type concept ID relationship link to the "
        "concept lookup table."
    )
    modifier = relationship(
        'ConceptId',
        foreign_keys=[modifier_id],
        back_populates='stated_rel_modifier',
        doc="The modifier concept ID relationship link to the concept lookup "
        "table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'relationship_id',
                'effective_time',
                'module_id',
                name='desc_status_uc'
            ),
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctTextDef(Base):
    """A representation of the snomed clinical concept text definiton table.

    Parameters
    ----------
    td_pk : `int`
        An auto-incremented primary key.
    text_def_id : `int`
        Link back to the text definitions lookup table. This is indexed.
        Foreign key to ``sctid_text_def.text_def_id``.
    effective_time : `date.date`
        The date for the text definition.
    is_active : `bool`
        Is the text definition active.
    module_id : `int`
        The module ID for the text definition. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    concept_id : `int`
        The snomed concept ID that maps to the text definition. This is
        indexed. Foreign key to ``sctid_concept.concept_id``.
    language_code : `str`
        The language code for the text definition (length 2).
    type_id : `int`
        The type mapping for the text definition. This is indexed. Foreign key
        to ``sctid_concept.concept_id``.
    term : `str`
        The actual text definition text. This is indexed (length text).
    case_significance_id : `int`
        The case significance identifier for the text definition. This is
        indexed. Foreign key to ``sctid_concept.concept_id``.
    sctid_text_def : `snomed_ct.orm.TextDefId`, optional, default: `NoneType`
        The text definition ID relationship link to the text definition lookup
        table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The module concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The type concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The case significance concept ID relationship link to the concept
        lookup table.

    Notes
    -----
    This is different from the text_def_id lookup table and is only guaranteed
    to be unique across ``text_def_id``, ``effective_time``, ``module_id``.
    This differs from the sct2_descriptions table in that it has longer form
    text descriptions of the concepts and does not have full coverage. It also
    only has data from the snomed ct international edition and not from the UK
    modules.

    See also
    --------
    snomed_ct.orm.TextDefId
    snomed_ct.orm.ConceptId
    """
    __tablename__ = 'sct2_text_def'

    td_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key"
    )
    text_def_id = Column(
        BigInteger, ForeignKey('sctid_text_def.text_def_id'),
        nullable=False, index=True,
        doc="Link back to the text definitions lookup table."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the text definition."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the text definition active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the text definition."
    )
    concept_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The snomed concept ID that maps to the text definition."
    )
    language_code = Column(
        String(2), nullable=False,
        doc="The language code for the text definition."
    )
    type_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The type mapping for the text definition."
    )
    term = Column(
        Text, nullable=False, index=True,
        doc="The actual text definition text."
    )
    case_significance_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The case significance identifier for the text definition."
    )

    # -------------------------------------------------------------------------~
    text_def = relationship(
        'TextDefId',
        foreign_keys=[text_def_id],
        back_populates='text_def',
        doc="The text definition ID relationship link to the text definition"
        " lookup table."
    )
    concept = relationship(
        'ConceptId',
        foreign_keys=[concept_id],
        back_populates='text_def',
        doc="The concept ID relationship link to the concept lookup table."
    )
    module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='text_def_module',
        doc="The module concept ID relationship link to the concept lookup "
        "table."
    )
    text_def_type = relationship(
        'ConceptId',
        foreign_keys=[type_id],
        back_populates='text_def_type',
        doc="The type concept ID relationship link to the concept lookup "
        "table."
    )
    case_significance = relationship(
        'ConceptId',
        foreign_keys=[case_significance_id],
        back_populates='text_def_case_significance',
        doc="The case significance concept ID relationship link to the concept"
        " lookup table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'text_def_id',
                'effective_time',
                'module_id',
                name='desc_status_uc'
            ),
            Index(
                'text_term_idx', term,
                mysql_prefix='FULLTEXT',
                # mariadb_prefix='FULLTEXT'
            )
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DerRefsetSimple(Base):
    """A representation of the simple reference set table:
    ``der2_refset_simple``.

    Parameters
    ----------
    drs_pk : `int`
        An auto-incremented primary key.
    hex_id : `str`
        This is a 36 character long hexadecimal ID included in the file. I
        think it is designed as a primary key and seems to be unique (length
        36).
    effective_time : `date.date`
        The date for the reference set row definition.
    is_active : `bool`
        Is the reference set row active.
    module_id : `int`
        The module ID for the reference set row. This is indexed. Foreign key
        to ``sctid_concept.concept_id``.
    refset_id : `int`
        The reference set concept identifier that maps to the concepts table.
        This is indexed. Foreign key to ``sctid_concept.concept_id``.
    refset_comp_id : `int`
        The reference set component concept identifier that maps to the
        concepts table. This is indexed. Foreign key to
        ``sctid_concept.concept_id``.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The module concept ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The refset ID relationship link to the concept lookup table.
    sctid_concept : `snomed_ct.orm.ConceptId`, optional, default: `NoneType`
        The refset component ID relationship link to the concept lookup table.

    Notes
    -----
    This is imported from the ``der2_Refset_SimpleMONOSnapshot_GB_<date>.txt``
    file.
    """
    __tablename__ = 'der2_refset_simple'

    drs_pk = Column(
        Integer, nullable=False, primary_key=True,
        doc="An auto-incremented primary key"
    )
    hex_id = Column(
        String(36),
        nullable=False,
        doc="This is a 36 character long hexadecimal ID included in the file."
        " I think it is designed as a primary key and seems to be unique."
    )
    effective_time = Column(
        Date, nullable=False,
        doc="The date for the reference set row definition."
    )
    is_active = Column(
        Boolean, nullable=False,
        doc="Is the reference set row active."
    )
    module_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The module ID for the reference set row."
    )
    refset_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The reference set concept identifier that maps to the concepts "
        "table."
    )
    refset_comp_id = Column(
        BigInteger, ForeignKey('sctid_concept.concept_id'),
        nullable=False, index=True,
        doc="The reference set component concept identifier that maps to the"
        " concepts table."
    )

    # -------------------------------------------------------------------------~
    module = relationship(
        'ConceptId',
        foreign_keys=[module_id],
        back_populates='simple_refset_module',
        doc="The module concept ID relationship link to the concept lookup "
        "table."
    )
    refset = relationship(
        'ConceptId',
        foreign_keys=[refset_id],
        # back_populates='simple_refset',
        doc="The refset ID relationship link to the concept lookup table."
    )
    refset_comp = relationship(
        'ConceptId',
        foreign_keys=[refset_id],
        # back_populates='simple_refset_comp',
        doc="The refset component ID relationship link to the concept lookup"
        " table."
    )
    # -------------------------------------------------------------------------~

    # -------------------------------------------------------------------------~
    __table_args__ = (
        (
            UniqueConstraint(
                'hex_id',
                'effective_time',
                'module_id',
                name='ref_simple_uc'
            ),
        )
    )
    # -------------------------------------------------------------------------~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ConceptId(Base):
    """A representation of the snomed clinical concepts lookup table.

    Parameters
    ----------
    concept_id : `int`
        Unique snomed concept IDs. Although auto-increment, these are actually
        imported.
    sct2_concept : `snomed_ct.orm.SctConcept`, optional, default: `NoneType`
        The concept ID relationship link to the sct2_concept table.
    sct2_concept : `snomed_ct.orm.SctConcept`, optional, default: `NoneType`
        The module concept ID relationship link to the sct2_concept table.
    sct2_concept : `snomed_ct.orm.SctConcept`, optional, default: `NoneType`
        The definition status concept ID relationship link to the sct2_concept
        table.
    sct2_description : `snomed_ct.orm.SctDescription`, optional, \
    default: `NoneType`
        The concept ID relationship link to the sct2_description table.
    sct2_description : `snomed_ct.orm.SctDescription`, optional, \
    default: `NoneType`
        The concept ID relationship link to the sct2_description table.
    sct2_description : `snomed_ct.orm.SctDescription`, optional, \
    default: `NoneType`
        The type concept ID relationship link to the sct2_description table.
    sct2_description : `snomed_ct.orm.SctDescription`, optional, \
    default: `NoneType`
        The case significance concept ID relationship link to the
        sct2_description table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The module concept ID relationship link to the sct2_relationship table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The source concept ID relationship link to the sct2_relationship table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The destination concept ID relationship link to the sct2_relationship
        table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The type concept ID relationship link to the sct2_relationship table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The characteristic type concept ID relationship link to the
        sct2_relationship table.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, \
    default: `NoneType`
        The modifier concept ID relationship link to the sct2_relationship
        table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The module concept ID relationship link to the sct2_stated_relationship
        table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The source concept ID relationship link to the sct2_stated_relationship
        table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The destination concept ID relationship link to the
        sct2_stated_relationship table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The type concept ID relationship link to the sct2_stated_relationship
        table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The characteristic type concept ID relationship link to the
        sct2_stated_relationship table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, \
    optional, default: `NoneType`
        The modifier concept ID relationship link to the
        sct2_stated_relationship table.
    sct2_text_def : `snomed_ct.orm.SctTextDef`, optional, default: `NoneType`
        The concept ID relationship link to the sct2_text_def table.
    sct2_text_def : `snomed_ct.orm.SctTextDef`, optional, default: `NoneType`
        The module concept ID relationship link to the sct2_text_def table.
    sct2_text_def : `snomed_ct.orm.SctTextDef`, optional, default: `NoneType`
        The type concept ID relationship link to the sct2_text_def table.
    sct2_text_def : `snomed_ct.orm.SctTextDef`, optional, default: `NoneType`
        The case significance concept ID relationship link to the sct2_text_def
        table.
    der2_refset_simple : `snomed_ct.orm.DerRefsetSimple`, optional, default:\
    `NoneType`
        The relationship link to the module ID of the simple refset table.
    der2_refset_simple : `snomed_ct.orm.DerRefsetSimple`, optional, default:\
    `NoneType`
        The relationship link to the refset ID of the simple refset table.
    der2_refset_simple : `snomed_ct.orm.DerRefsetSimple`, optional, default:\
    `NoneType`
        The relationship link to the refset component ID of the simple refset
        table.

    Notes
    -----
    This just acts as an endpoint for unique snomed concepts so other tables
    integrity can be verified.
    """
    __tablename__ = 'sctid_concept'

    concept_id = Column(
        BigInteger, nullable=False, primary_key=True,
        doc="Unique snomed concept IDs. Although auto-increment, these are"
        " actually imported"
    )

    # -------------------------------------------------------------------------#
    concept = relationship(
        'SctConcept',
        foreign_keys=[SctConcept.concept_id],
        back_populates='concept',
        doc="The concept ID relationship link to the sct2_concept table."
    )
    concept_module = relationship(
        'SctConcept',
        foreign_keys=[SctConcept.module_id],
        back_populates='module',
        doc="The module concept ID relationship link to the sct2_concept "
        "table."
    )
    concept_definition_status = relationship(
        'SctConcept',
        foreign_keys=[SctConcept.definition_status_id],
        back_populates='definition_status',
        doc="The definition status concept ID relationship link to the "
        "sct2_concept table."
    )
    description = relationship(
        'SctDescription',
        foreign_keys=[SctDescription.concept_id],
        back_populates='concept',
        doc="The concept ID relationship link to the sct2_description table."
    )
    desc_module = relationship(
        'SctDescription',
        foreign_keys=[SctDescription.module_id],
        back_populates='desc_module',
        doc="The concept ID relationship link to the sct2_description table."
    )
    desc_type = relationship(
        'SctDescription',
        foreign_keys=[SctDescription.type_id],
        back_populates='desc_type',
        doc="The type concept ID relationship link to the sct2_description "
        "table."
    )
    desc_case_significance = relationship(
        'SctDescription',
        foreign_keys=[SctDescription.case_significance_id],
        back_populates='desc_case_significance',
        doc="The case significance concept ID relationship link to the "
        "sct2_description table."
    )
    rel_module = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.module_id],
        back_populates='module',
        doc="The module concept ID relationship link to the sct2_relationship"
        " table."
    )
    rel_source = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.source_id],
        back_populates='source',
        doc="The source concept ID relationship link to the sct2_relationship"
        " table."
    )
    rel_destination = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.destination_id],
        back_populates='destination',
        doc="The destination concept ID relationship link to the "
        "sct2_relationship table."
    )
    rel_type = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.type_id],
        back_populates='rel_type',
        doc="The type concept ID relationship link to the sct2_relationship"
        " table."
    )
    rel_char_type = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.characteristic_type_id],
        back_populates='char_type',
        doc="The characteristic type concept ID relationship link to the "
        "sct2_relationship table."
    )
    rel_modifier = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.modifier_id],
        back_populates='modifier',
        doc="The modifier concept ID relationship link to the "
        "sct2_relationship table."
    )
    stated_rel_module = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.module_id],
        back_populates='module',
        doc="The module concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    stated_rel_source = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.source_id],
        back_populates='source',
        doc="The source concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    stated_rel_destination = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.destination_id],
        back_populates='destination',
        doc="The destination concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    stated_rel_type = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.type_id],
        back_populates='stated_rel_type',
        doc="The type concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    stated_rel_char_type = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.characteristic_type_id],
        back_populates='char_type',
        doc="The characteristic type concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    stated_rel_modifier = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.modifier_id],
        back_populates='modifier',
        doc="The modifier concept ID relationship link to the "
        "sct2_stated_relationship table."
    )
    text_def = relationship(
        'SctTextDef',
        foreign_keys=[SctTextDef.concept_id],
        back_populates='concept',
        doc="The concept ID relationship link to the sct2_text_def table."
    )
    text_def_module = relationship(
        'SctTextDef',
        foreign_keys=[SctTextDef.module_id],
        back_populates='module',
        doc="The module concept ID relationship link to the sct2_text_def "
        "table."
    )
    text_def_type = relationship(
        'SctTextDef',
        foreign_keys=[SctTextDef.type_id],
        back_populates='text_def_type',
        doc="The type concept ID relationship link to the sct2_text_def table."
    )
    text_def_case_significance = relationship(
        'SctTextDef',
        foreign_keys=[SctTextDef.case_significance_id],
        back_populates='case_significance',
        doc="The case significance concept ID relationship link to the "
        "sct2_text_def table."
    )
    simple_refset_module = relationship(
        'DerRefsetSimple',
        foreign_keys=[DerRefsetSimple.module_id],
        back_populates='module',
        doc="The relationship link to the module ID of the simple refset "
        "table."
    )
    simple_refset = relationship(
        'DerRefsetSimple',
        foreign_keys=[DerRefsetSimple.refset_id],
        back_populates='refset',
        doc="The relationship link to the refset ID of the simple refset "
        "table."
    )
    simple_refset_comp = relationship(
        'DerRefsetSimple',
        foreign_keys=[DerRefsetSimple.refset_id],
        back_populates='refset_comp',
        doc="The relationship link to the refset component ID of the simple"
        " refset table."
    )
    version_lang = relationship(
        'VersionLanguage',
        foreign_keys=[VersionLanguage.concept_id],
        back_populates='concept',
        doc="The relationship link to the version language concept ID of the"
        " version language table."
    )
    version_module = relationship(
        'VersionModule',
        foreign_keys=[VersionModule.concept_id],
        back_populates='concept',
        doc="The relationship link to the version module concept ID of the"
        " version module table."
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DescriptionId(Base):
    """A representation of the snomed clinical concept description ID table.

    Parameters
    ----------
    description_id : `int`
        Although auto-increment, these are actually imported.
    sct2_description : `snomed_ct.orm.SctDescription`, optional, default: `NoneType`
        The unique ID relationship link to the sct2_description table.

    Notes
    -----
    This just acts as an endpoint for unique snomed concepts occurring in the
    descriptions table.
    """
    __tablename__ = 'sctid_description'

    description_id = Column(
        BigInteger, nullable=False, primary_key=True,
        doc="Although auto-increment, these are actually imported"
    )

    # -------------------------------------------------------------------------#
    description = relationship(
        'SctDescription',
        foreign_keys=[SctDescription.description_id],
        back_populates='description',
        doc="The unique ID relationship link to the sct2_description table."
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RelationshipId(Base):
    """A representation of the snomed clinical relationship ID table.

    Parameters
    ----------
    relationship_id : `int`
        Although auto-increment, these are actually imported.
    sct2_relationship : `snomed_ct.orm.SctRelationship`, optional, default: `NoneType`
        The unique ID relationship link to the sct2_relationship table.
    sct2_stated_relationship : `snomed_ct.orm.SctStatedRelationship`, optional, default: `NoneType`
        The unique ID relationship link to the sct2_stated_relationship table.

    Notes
    -----
    This just acts as an endpoint for unique snomed concepts occurring in the
    relationships. This is formed of the unique relationship Ids across the
    ``sct2_relationship`` and ``sct2_stated_relationship`` tables.
    """
    __tablename__ = 'sctid_relationship'

    relationship_id = Column(
        BigInteger, nullable=False, primary_key=True,
        doc="Although auto-increment, these are actually imported"
    )

    # -------------------------------------------------------------------------#
    rel = relationship(
        'SctRelationship',
        foreign_keys=[SctRelationship.relationship_id],
        back_populates='rel',
        doc="The unique ID relationship link to the sct2_relationship table."
    )
    stated_rel = relationship(
        'SctStatedRelationship',
        foreign_keys=[SctStatedRelationship.relationship_id],
        back_populates='rel',
        doc="The unique ID relationship link to the sct2_stated_relationship"
        " table."
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TextDefId(Base):
    """A representation of the snomed text definition lookup table.

    Parameters
    ----------
    text_def_id : `int`
        Although auto-increment, these are actually imported.
    sct2_text_def : `snomed_ct.orm.SctTextDef`, optional, default: `NoneType`
        The unique ID relationship link to the sct2_text_def table.

    Notes
    -----
    This just acts as an endpoint for unique snomed concepts occurring in the
    text definitions.
    """
    __tablename__ = 'sctid_text_def'

    text_def_id = Column(
        BigInteger, nullable=False, primary_key=True,
        doc="Although auto-increment, these are actually imported"
    )

    # -------------------------------------------------------------------------#
    text_def = relationship(
        'SctTextDef',
        foreign_keys=[SctTextDef.text_def_id],
        back_populates='text_def',
        doc="The unique ID relationship link to the sct2_text_def table."
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)
