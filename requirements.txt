pymysql
pytest
pytest-dependency
sqlalchemy<1.4
sqlalchemy-utils
tqdm

# For merge-sort
sortedcontainers

# For pyaddons
biopython

# My stuff
git+https://gitlab.com/cfinan/merge-sort.git@master#egg=merge-sort
git+https://gitlab.com/cfinan/sqlalchemy-config.git@master#egg=sqlalchemy-config
git+https://gitlab.com/cfinan/pyaddons.git@master#egg=pyaddons
