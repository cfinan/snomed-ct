===============
SnomedCT schema
===============

The schema used by snomed-ct is taken from that used b the NHS documented in the implementations PDF file ``doc_UKSnomedCTTechnicalOverview_Current-en-GB_GB1000000_20210519.pdf``, or more generally ``doc_UKSnomedCTTechnicalOverview_Current-en-GB_<version>.pdf``. This is available `here <https://nhsengland.kahootz.com/t_c_home/view?objectId=16607376>`_.

Below is a diagram of the currently implemented schema with the ``concept_id`` links between the tables shown in green.

.. image:: ./_static/snomed-ct.png
   :width: 500
   :alt: snomed-ct schema
   :align: center

.. include:: ./data_dict/snomed-ct-tables.rst
