===============
Common concepts
===============

Some concept terms occur multiple times in many tables. These seem to add some context to the concept that they are associated with.

As a learning exercise and reference, these are listed here for the monolith edition.


``module_id``
-------------

This is an indicator of the source module that the SnomedCT concept is derived from. This occurs in the following tables:

* ``sct2_concept``
* ``sct2_description``
* ``sct2_relationship``
* ``sct2_stated_relationship``
* ``sct2_text_def``

The descriptions for these are as follows:

.. include:: ./data_dict/module-ids.rst


``case_significance_id``
------------------------

This occurs in the following tables:

* ``sct2_description``
* ``sct2_text_def``

The descriptions for these are as follows:

.. include:: ./data_dict/case-signif-ids.rst


``characteristic_type_id``
--------------------------

This occurs in the following tables:

* ``sct2_relationship``
* ``sct2_stated_relationship``

The descriptions for these are as follows:

.. include:: ./data_dict/char-type-ids.rst


``definition_status_id``
------------------------

This occurs in the following table:

* ``sct2_concept``

The descriptions for these are as follows:

.. include:: ./data_dict/def-status-ids.rst

``type_id``
-----------

This occurs in the following tables:

* ``sct2_description``
* ``sct2_relationship``
* ``sct2_stated_relationship``
* ``sct2_text_def``

The descriptions for these are as follows:

.. include:: ./data_dict/type-ids.rst
