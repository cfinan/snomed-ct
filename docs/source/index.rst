.. snomed-ct documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SnomedCt builder
================

`snomed-ct <https://gitlab.com/cfinan/snomed-ct>`_ is a package to enable the building of the SnomedCT distributions that can be downloaded from the NHS TRUD site. Note, it is intended for research purposes only and not healthcare implementations.

Currently the core tables are implemented and I am gradually making my way through the refset tables, trying to understand them as I go.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started
   input_files
   common_concepts

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples/using_the_orm

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts/python_scripts
   api/build

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
