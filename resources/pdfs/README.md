# PDFs

This contains PDF documentation that has been downloaded directly from the [TRUD site](https://isd.digital.nhs.uk/trud/users/guest/filters/0/home). Specifically, the [clinical extension]([https://nhsengland.kahootz.com/t_c_home/view?objectId=16607376) and also [drug extension](https://nhsengland.kahootz.com/t_c_home/view?objectId=14540272). I use these for my reference, I will try to keep them current but please check the TRUD site for updates.

The [technical resources](https://nhsengland.kahootz.com/t_c_home/view?objectId=37788016) site also has some good information.
