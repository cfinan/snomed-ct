==============
Python scripts
==============

.. _snomedct_build:

``snomed-ct-build``
-------------------

.. argparse::
   :module: snomed_ct.build
   :func: _init_cmd_args
   :prog: snomed-ct-build

``snomed-ct-munge``
-------------------

.. argparse::
   :module: snomed_ct.munge
   :func: _init_cmd_args
   :prog: snomed-ct-munge

Example command
~~~~~~~~~~~~~~~

If the clinical, drug and monolith edition have been downloaded the into their respective directories under ``/data/snomedct/v36.2`` and unzipped. A munge edition can be built as follows:

.. code-block:: console

   $ snomed-ct-munge -vv --outdir munge/ --tmp /data/snomedct /data/snomedct/v36.2/drug/* /data/snomedct/v36.2/monolith/* /data/snomedct/v36.2/clinical/* /data/snomedct/v36.2/snomed-ct-36.2-munge.db
   === snomed-ct-munge (snomed_ct v0.1.1a0) ===
   [info] 17:00:27 > config value: /home/rmjdcfi/.db_conn.cnf
   [info] 17:00:27 > config_section value: snomed_ct
   [info] 17:00:27 > db_url value: None
   [info] 17:00:27 > exists value: False
   [info] 17:00:27 > indirs length: 11
   [info] 17:00:27 > outdir value: /data/snomedct/v36.2/munge
   [info] 17:00:27 > tmp value: /data/snomedct
   [info] 17:00:27 > verbose value: 2
   [info] 17:00:28 > reading release info...
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:28 > using indir: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:28 > munging concepts #releases: 7
   [info] 17:00:28 > processing concepts: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:41 > processing concepts: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:00:41 > processing concepts: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:01:08 > processing concepts: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:01:23 > processing concepts: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:01:23 > processing concepts: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:01:28 > processing concepts: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:02:07 > found # unique concepts: 1161037
   [info] 17:02:07 > munging descriptions #releases: 7
   [info] 17:02:07 > processing descriptions: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:03:08 > processing descriptions: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:03:08 > processing descriptions: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:05:06 > processing descriptions: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:06:40 > processing descriptions: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:06:40 > processing descriptions: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:06:49 > processing descriptions: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:44 > found # unique descriptions: 3105427
   [info] 17:09:44 > munging text definitions #releases: 7
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:44 > processing text definitions: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:09:46 > found # unique text definitions: 10212
   [info] 17:09:46 > munging relationships #releases: 7
   [info] 17:09:46 > processing relationships: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:13:50 > processing relationships: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:13:50 > processing relationships: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:19:20 > processing relationships: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:22:50 > processing relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:22:50 > processing relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:23:08 > processing relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:30:42 > found # unique relationships: 8115719
   [info] 17:30:42 > munging stated relationships #releases: 7
   [info] 17:30:42 > processing stated relationships: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:30:42 > processing stated relationships: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:30:42 > processing stated relationships: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:30:42 > processing stated relationships: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:31:51 > processing stated relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:31:51 > processing stated relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:31:51 > processing stated relationships: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:32:30 > found # unique stated relationships: 1026165
   [info] 17:32:30 > munging simple refsets #releases: 7
   [info] 17:32:30 > processing simple refsets: /data/snomedct/v36.2/drug/SnomedCT_UKDrugRF2_PRODUCTION_20230607T000001Z
   [info] 17:33:16 > processing simple refsets: /data/snomedct/v36.2/drug/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:33:18 > processing simple refsets: /data/snomedct/v36.2/monolith/SnomedCT_MonolithRF2_PRODUCTION_20230607T120000Z
   [info] 17:34:31 > processing simple refsets: /data/snomedct/v36.2/clinical/SnomedCT_InternationalRF2_PRODUCTION_20230131T120000Z
   [info] 17:34:31 > processing simple refsets: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_20230607T000001Z
   [info] 17:35:04 > processing simple refsets: /data/snomedct/v36.2/clinical/SnomedCT_UKClinicalRF2_PRODUCTION_20230607T000001Z
   [info] 17:35:04 > processing simple refsets: /data/snomedct/v36.2/clinical/SnomedCT_UKEditionRF2_PRODUCTION_20230607T000001Z
   [info] 17:36:40 > found # unique simple refsets: 2113615
   [info] 17:36:40 > moving to outdir: /data/snomedct/v36.2/munge...
   [info] 17:36:40 > build location: /data/snomedct/v36.2/munge/SnomedCT_MungeRF2_ALPHA_202307050000000Z
   [info] 18:06:46 > *** END ***


