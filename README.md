# SnomedCT

__version__: `0.1.2a0`

The snomed-ct package is a toolkit to to build the NHS SnomedCT distributions that can be obtained from the [NHS TRUD site](https://isd.digital.nhs.uk/trud/users/authenticated/filters/0/categories/26/items/1799/releases).

This project is incomplete and I am gradually adding tables to the schema. It uses SQLALchemy but so far has only been tested against SQLite, although it should be relatively straight forward to adapt to other relational databases.

**NOTE** This is for research purposes only and has only been tested to build the release format 2 files from the *monolith* distribution. In theory it can build the *clinical* and *drug* distributions, however, it might run into issues as they are distributed in separate directories that *I think* collectively make up the distribution.

If you want everything, you can download the *monolith*, *clinical* and *drug* editions and run the munge script. This is a similar idea to the *monolith* edition but can be run against the full release, where as the *monolith* edition is only a snapshot release.

There is [online](https://cfinan.gitlab.io/snomed-ct/index.html) documentation for snomed-ct.

## Installation instructions
At present, snomed-ct is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install via conda unless you have access to gwas-norm, in which case you can use a pip install from the git repository.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge snomed-ct
```

There are currently builds for Python v3.8, v3.9, v3.10 for Linux-64 and Mac-osX.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/snomed-ct.git
cd snomed-ct
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.
