======================================
SnomedCT distributions and input files
======================================

This contains an overview of the different distributions, editions, releases and table files I have tested against so far. I am not 100% sure in the significance of all of them.

Releases
--------

The NHS distributes up to three different releases of each edition of SnomedCT. A `full` release that contains all current and historic records going back to 2004 for the UK and 2002 for the international edition. A `snapshot` release that contains contains only the most recent concepts and a `delta` release that contains only those concepts added (or changed?) since the previous release. As far as I can tell the structures are the same between them.

Distributions and editions
--------------------------

Three distributions (my terminology) seem to be available in TRUD. The `monolith` distribution, the `clinical` distribution and the `drug` distribution. I have also seen reference to a `pathology` one but can't seem to find it? Within these, there are one or more editions (my terminology) which can contain one or more releases.

Monolith
~~~~~~~~

The monolith distribution, which contains a single monolith edition and a single release, the snapshot release. This is an amalgamation of the current SnomedCT international version combined with the UK clinical, UK drug and UK pathology sets, with the various duplicates removed.

Clinical
~~~~~~~~

The clinical distribution contains 4 editions, these are listed below with the releases for each of them.

* ``SnomedCT_InternationalRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` release.
* ``SnomedCT_UKClinicalRefsetsRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` and `delta` release. This seems to contain very few concepts and a lot of simple reference sets.
* ``SnomedCT_UKClinicalRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` and `delta` release. Only has a subset of the concepts (the UK set?), for example the April 2023 full version 36.0 has 97,357 unique concepts compared to the ~1M in the monolith edition.
* ``SnomedCT_UKEditionRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` and `delta` release. This contains very few concepts, for example the April 2023  full version 36.0 has 2,778 unique concepts compared to the ~1M in the monolith edition.


Drug
~~~~

The drug distribution contains 2 editions, these are listed below with their releases.

* ``SnomedCT_UKDrugRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` and `delta` release. This contains many concepts, for example the May 2023 full version 36.1 contains 476,541 unique concepts.
* ``SnomedCT_UKEditionRF2_PRODUCTION_<date as YYYMMDD><other digits and characters>Z`` - This contains the `full` and `snapshot` and `delta` release. This seems to contain very few concepts and a lot of simple reference sets. This has very few concepts and seems to be the same as the similarly named edition in the Clinical distribution, as the May 2023 full version 36.1 contains 2,778 unique concepts.

Table files
-----------

Each distribution is downloaded as a zip archive. The zip archive should have the following structure ``uk_sct2<distribution>_<major version>.<minor version>.<release>_<date as YYYYMMDD><other digits and numbers>.zip``. The distributions I have encountered and imported are ``mo`` (monolith), ``cl`` (clinical) and ``dr`` (drug), here is an example for the monolith distribution ``uk_sct2mo_36.1.0_20230510000001Z.zip``.

The zip archive should be extracted and will contain one of more edition directories, these are the directory paths that should be given to ``snomed-ct-build`` :ref:`script <snomedct_build>`. Within these are the releases (`full`, `snapshot`, `delta`), you specify the release you want to ``snomed-ct-build``. Each release contains multiple files split over two directories, ``Terminology`` and ``Refset``.

The file directory names should not be changed otherwise ``snomed-ct-build`` will not be able to find the appropriate files. The file name structures and some information that I have gather so far is shown below.

The directory structure is referenced relative to a root directory of the edition that has the name ``SnomedCT_<edition>RF2_PRODUCTION_<date as YYYYMMDD><other digits and numbers>``, i.e. ``SnomedCT_MonolithRF2_PRODUCTION_20230510T120000Z``.

The information here is what I have observed from handling the files and reading the docs on TRUD, however, I am not an expert and it is likely to be incomplete. The technical overview describes a section in a documentation that I can't find - `'Note: please review section “5.4. SNOMED CT - File Naming Conventions” within the “SNOMED CT Technical Implementation Guide” document to review the file naming convention which also details the <> qualifiers used in this table'`. This looks like it might be very useful.

As I understand it, the core release files are within The ``<release>/Terminology`` directories and should be useful for most common tasks, these are all implemented. The ``<release>/Refset`` directory contains reference set files that are also referred to as subsets or value sets, the simple reference sets (refset) in the release are a collection of SnomedCT concepts designed for a specific purpose such as to restrict data entry or data reporting i.e. for a national dataset. These are not fully implemented at present but I am gradually making my way through them. With that in mind, my interpretation of the contents of the directory are as follows:

* ``Readme-en.txt`` - A readme for the release. This has information about the release cycles for the various components of the monolith edition. These are the drug extension, the clinical extension and the UK pathology and laboratory medicine extension/
* ``release_package_information.json`` - A JSON structured file detailing the modules in the release and the version date. The version date is used directly by the build script.
* ``<release>/Terminology/sct2_Identifier_<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - This is empty in the releases I have looked at, not sure of the significance of this table. This is not currently used.
* ``<release>/Terminology/sct2_Description_<edition><release>-en_<GB or INT>_<date as YYYYMMDD>.txt`` - The various descriptions for the Snomed concepts, there can be >1 description for each concept. This is imported.
* ``<release>/Terminology/sct2_Relationship_<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - The relationships between the concepts. Different relationship types exist within SnomedCT. This is imported.
* ``<release>/Terminology/sct2_StatedRelationship_<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - The stated relationship. This has no data in it at present and I am not sure on the differences between this and the relationship table but they have the same headers. This is imported even though it is empty.
* ``<release>/Terminology/sct2_Concept_<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - The core Snomed concepts table. The id field in this table is unique in the examples I have looked at but from the docs it is treated as if they are not. This is imported.
* ``<release>/Terminology/sct2_sRefset_OWLExpression<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - An OWL formatted file that presumably contains the relationships between the concepts. This is not currently used.
* ``<release>/Terminology/sct2_TextDefinition_<edition><release>-en_<GB or INT>_<date as YYYYMMDD>.txt`` - The text definitions of the concepts. These are longer form descriptions of the concepts and the table has the same structure as the descriptions table. This only contains a subset of snomed codes and I think these are drawn from the international edition only, not the UK components. This is imported.
* ``<release>/Refset/Content/der2_cRefset_AttributeValue<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented
* ``<release>/Refset/Content/der2_Refset_Simple<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, although is implemented.
* ``<release>/Refset/Content/der2_cRefset_Association<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_ssRefset_ModuleDependency<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_cciRefset_RefsetDescriptor<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_sssssssRefset_MRCMDomain<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_ciRefset_DescriptionType<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_cRefset_MRCMModuleScope<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_ssccRefset_MRCMAttributeRange<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Metadata/der2_cissccRefset_MRCMAttributeDomain<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Map/der2_iisssccRefset_ExtendedMap<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Map/der2_iissscRefset_ComplexMap<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Map/der2_sRefset_SimpleMap<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Map/der2_iisssciRefset_ExtendedMap<edition><release>_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.
* ``<release>/Refset/Language/der2_cRefset_Language<edition><release>-en_<GB or INT>_<date as YYYYMMDD>.txt`` - Not understood, not currently implemented.

