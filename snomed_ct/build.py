"""Build a relational database from the SnomedCT monolith edition files on
TRUD.

This is designed to built the database from scratch so it should not exist of
exist but with empty tables (or no tables).
"""
from snomed_ct import (
    __version__,
    __name__ as pkg_name,
    common as common,
    orm as orm,
)
from sqlalchemy_config import config as cfg
from pyaddons import log
# from sqlalchemy import and_, or_
# from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from tqdm import tqdm
from zipfile import ZipFile
from merge_sort import chunks, merge
import shutil
import glob
import argparse
import re
import os
import sys
import json
import csv
import tempfile
import pprint as pp


_PROG_NAME = 'snomed-ct-build'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""
# DELIMITER = "\t"
# """The delimiter of the token file (`str`).
# """
# MISSING = ['.', '', None]
# """Missing values in the token file (`list` of `str` or `NoneType`).
# """


csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``snmed_ct.build.build_snomed_ct``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    sm = cfg.get_sessionmaker(
        args.config_section, common._DEFAULT_PREFIX, url_arg=args.db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    session = sm()

    # Have we been given a directory or a file.
    try:
        open(args.input).close()
        raise ValueError(
            f"expected an input directory not a file: {args.input}"
        )
        # file_info = get_zip_file_data(args.input)
        # logger.info(f"extracting zip file: {file_info.path}")
        # indir = extract_data(file_info.path, args.tmp)
    except IsADirectoryError:
        # we have a directory so all good
        indir = args.input

    try:
        # Make sure the database exists (or does not exist)
        cfg.create_db(sm, exists=args.exists)

        # Build the SnomedCT release
        build_snomed_ct(
            session, indir, verbose=args.verbose, tmpdir=args.tmp,
            release=args.release
        )
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'input',
        type=str,
        help="The input directory from the zip archive, the names should not"
        " be changed and exactly as unzipped. Extraction directly from the zip"
        " archive is not supported any more as there are multiple editions in"
        " some archives."
    )
    parser.add_argument(
        'db_url',
        nargs='?',
        type=str,
        help="The database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    # parser.add_argument(
    #     '-V', '--version', default="monolith", type=str,
    #     choices=["monolith", "drug"],
    #     help="The version, either, monolith, drug, clinical, path. Currently"
    #     " only monolith and drug are implemented."
    # )
    parser.add_argument(
        '-R', '--release', default=common.FULL_RELEASE.lower(), type=str,
        choices=[i.lower() for i in common.ALLOWED_RELEASES],
        help="The release you want to build, either, snapshot, full, delta."
        "Please note, not all releases are available for all editions. For "
        "example, the monolith edition is only snapshot."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    parser.add_argument(
        '-e', '--exists',  action="store_true",
        help="An empty database already exists, so will not be created."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.expanduser(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_snomed_ct(session, root_dir, verbose=False, tmpdir=None,
                    release=common.FULL_RELEASE):
    """Add a DM+D database version to the prescription database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object attached to the snimed CT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use for some sorting of files, if not provided the
        default system temp location is used.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    logger.info(f"using indir: {root_dir}")
    logger.info("reading release info...")
    release_info = common.extract_dir_info(root_dir)

    # Sanity check the dates from the directory and the release info file
    if release_info.date != release_info.dir_date:
        raise ValueError(
            "directory date and release date do not match:"
            f"{str(release_info.date)} vs. {str(release_info.dir_date)}"
        )

    logger.info(f"release date: {str(release_info.date)}")

    # Now make sure that the release we want is in the available releases
    release = release.title()
    if release not in release_info.release:
        raise ValueError(
            f"release ({','.join(release_info.release)}) does not "
            f"support release type: {release}"
        )

    logger.info("ensuring all snomed-ct tables are in place")
    common.create_tables(session)

    # TODO: Add back in
    v = orm.Version(
        version_date=release_info.date,
        delta_from_date=release_info.delta_from_date,
        delta_to_date=release_info.delta_to_date,
        edition=release_info.edition,
        release=release.lower(),
        version_type=release_info.version,
        dir_basename=os.path.basename(release_info.path),
    )
    session.add(v)
    session.commit()

    # Import the terminology section tables
    logger.info("importing concept lookups...")
    nconcepts = import_concept_lookup(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )
    logger.info(f"imported #unique-concepts: {nconcepts}")

    # Now the concept lookup is in place the version, language and module IDs
    # can be imported
    for i in release_info.modules:
        session.add(
            orm.VersionModule(
                concept_id=i, version=v
            )
        )
    for i in release_info.lang_refsets:
        session.add(
            orm.VersionLanguage(
                concept_id=i, version=v
            )
        )
    session.commit()

    logger.info("importing concepts...")
    import_concepts(session, root_dir, release_info.date, verbose=prog_verbose,
                    release=release)

    logger.info("importing description lookups...")
    import_description_lookup(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )
    logger.info("importing description text...")
    import_descriptions(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )

    logger.info("importing relationship lookups...")
    import_relationship_lookup(
        session, root_dir, release_info.date, verbose=prog_verbose,
        tmpdir=tmpdir, release=release
    )
    logger.info("importing stated relationship...")
    import_stated_relationships(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )
    logger.info("importing relationships...")
    import_relationships(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )

    logger.info("importing text definition lookup...")
    import_text_def_lookup(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )
    logger.info("importing text definitions...")
    import_text_defs(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )

    logger.info("importing simple reference sets...")
    import_simple_refset(
        session, root_dir, release_info.date, verbose=prog_verbose,
        release=release
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_simple_refset(session, root_dir, date, verbose=False,
                         release=common.FULL_RELEASE):
    """This imports the simple reference set file:
    ``der2_Refset_SimpleMONOSnapshot_GB_<date>.txt``

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implemented all of these yet:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1
    * Module id should be present in snapshot concept table
    * Module id should be a subtype of th e appropriate metadata concept
    * id, effectivetime and moduleid should be unique
    * Following fields may not be null: id, effective time, active, module id,
      refset id and component
    * id
    * id fields should be hexadecimal numbers in the format
      00000000-0000-0000-0000-000000000000
    * Refset id should be present in snapshot concept table
    * Refset id should be a subtype of the appropriate metadata concept
    * Referenced concept ids should be present in snapshot concept table
    * Referenced description ids should be present in snapshot description
      table
    * Referenced components should be valid concept or description ids
    * Refsets must have a refset descriptor in the snapshot release

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * There should be no more recent records in the full release with the same
      id field
    * All active refset members should be in the corresponding RF1 subset
    * All members of RF1 subsets should be active in corresponding refset

    Rules Specific to the Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Refset id, referenced id and effective time should be unique
    * Record should not predate its referenced module id
    * Record should not predate its referenced refset id
    * Record should not predate its referenced component id
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_der_simple_refset_file(
            root_dir, date, verbose=verbose, release=release
    ):
        rows.append(
            dict(
                hex_id=row['id'],
                effective_time=row['effectiveTime'],
                is_active=row['active'],
                module_id=row['moduleId'],
                refset_id=row['refsetId'],
                refset_comp_id=row['referencedComponentId']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.DerRefsetSimple,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.DerRefsetSimple,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_text_def_lookup(session, root_dir, date, verbose=False,
                           release=common.FULL_RELEASE):
    """This imports the text definition lookup table, with unique IDs in it.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    """
    idx = 0
    commit = 100000
    # prev_id = None
    rows = []
    seen_id = set()
    for row in common.read_sct_text_def_file(
            root_dir, date, verbose=verbose, release=release
    ):
        cur_id = row['id']
        if cur_id in seen_id:
            continue

        rows.append(
            dict(
                text_def_id=row['id']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.TextDefId,
                rows
            )
            rows = []
            idx = 0
        seen_id.add(cur_id)
    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.TextDefId,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_text_defs(session, root_dir, date, verbose=False,
                     release=common.FULL_RELEASE):
    """This imports the text definitions.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implemented all of these yet:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be between 100000 and 999999999999999999
    * Partition digits of id field should reflect component source and
      identifier type
    * Check digit in id field should be correct according to Verhoeff function
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1
    * Module id should be present in snapshot concept table
    * Module id should be a subtype of the appropriate metadata concept
    * id, effectivetime and moduleid should be unique
    * No field should be null
    * Parameterised QA for common fields
    * Concept id should exist in snapshot concept table
    * Language code must be a recognised type
    * Type id should exist in snapshot concept table
    * Type id should be a subtype of the appropriate metadata concept
    * Terms should not contain tabs
    * Terms should not contain linefeeds
    * Terms should not contain carriage returns
    * Case significance id should exist in snapshot concept table
    * Case significance id should be a subtype of the appropriate metadata
      concept

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * FSNs of active concepts should terminate with a semantic tag
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    * Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Concept id must exist in snapshot concept table
    * There should be no more recent records in the full release with the same
      id field

    Rules Specific to the Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Fields specified as immutable may not be changed
    * Record should not predate its referenced module id
    * Record should not predate its referenced concept id
    * Record should not predate its referenced type id
    * Record should not predate its referenced case significance id
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_sct_text_def_file(
            root_dir, date, verbose=verbose, release=release
    ):
        rows.append(
            dict(
                text_def_id=row['id'],
                effective_time=row['effectiveTime'],
                is_active=row['active'],
                module_id=row['moduleId'],
                concept_id=row['conceptId'],
                language_code=row['languageCode'],
                type_id=row['typeId'],
                term=row['term'],
                case_significance_id=row['caseSignificanceId']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.SctTextDef,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.SctTextDef,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_relationship_lookup(session, root_dir, date, verbose=False,
                               tmpdir=None, release=common.FULL_RELEASE):
    """The relationship lookup is a merge of the primary keys from the
    relationship and stated relationship tables.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    tmpdir : `str`, optional, default: `NoneType`
        A tmp directory to place temporary sort files when doing an external
        merge sort of the IDs from both tables. If not supplied then the system
        temp location is used.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    Currently (05/06/2023) the stated relationship table is empty, nevertheless
    this is written to account for it if/when it exists.

    This reads both tables to create a unique primary key lookup table.
    """
    chunk_dir = tempfile.mkdtemp(dir=tmpdir, prefix="rels_")
    try:
        with chunks.ExtendedChunks(chunk_dir, key=lambda x: int(x)) as chunker:
            for row in common.read_sct_rel_file(
                    root_dir, date, verbose=verbose, stated_rel=False,
                    release=release
            ):
                chunker.add_row(f"{row['id']}\n")

            for row in common.read_sct_rel_file(
                    root_dir, date, verbose=verbose, stated_rel=True,
                    release=release
            ):
                chunker.add_row(f"{row['id']}\n")

        kwargs = dict(
            disable=not verbose,
            leave=False,
            unit=" rel",
            desc="[info] loading relationships..."
        )

        with merge.IterativeHeapqMerge(
                chunker.chunk_file_list, tmpdir=chunk_dir, header=False,
                key=lambda x: int(x)
        ) as m:
            idx = 0
            commit = 100000
            prev_row = None
            rows = []
            for row in tqdm(m, **kwargs):
                row = int(row)
                if row == prev_row:
                    continue
                rows.append(
                    dict(
                        relationship_id=row
                    )
                )
                idx += 1
                if idx == commit:
                    session.bulk_insert_mappings(
                        orm.RelationshipId,
                        rows
                    )
                    rows = []
                    idx = 0
                prev_row = row
        if len(rows) > 0:
            session.bulk_insert_mappings(
                orm.RelationshipId,
                rows
            )
        session.commit()
    finally:
        shutil.rmtree(chunk_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_relationships(session, root_dir, date, verbose=False,
                         release=common.FULL_RELEASE):
    """This imports the relationships between different concepts.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implement all of these:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be between 100000 and 999999999999999999
    * Partition digits of id field should reflect component source and
      identifier type
    * Check digit in id field should be correct according to Verhoeff function
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1
    * Module id should be present in snapshot concept table
    * Module id should be a subtype of the appropriate metadata concept
    * id, effectivetime and moduleid should be unique
    * No field should be null
    * Source id should exist in snapshot concept table
    * Destination id should exist in snapshot concept table
    * Relationship group should be a non -negative integer
    * Characteristic type id should be a subtype of the appropriate metadata
      concept
    * Modifier id should be a subtype of the appropriate metadata concept
    * Relationships may not be reflexive
    * Grouped relationships should be defining but not hierarchical
    * Extension relationships may not define international concepts

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * Relationship triple should be unique within a group
    * Type id should exist in snapshot concept table
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Source concept must exist in snapshot concept table
    * Destination concept must exist in snapshot concept table
    * Type id must exist in snapshot concept table
    * Active relationships may not have an inactive source concept
    * Active relationships may not have an inactive destination concept
    * Active relationships may not have an inactive type id
    * Root concept should exist as a supertype (only) in the hierarchy
    * Relationship groups should contain more than one relationship
    * Ungrouped relationships may not also occur within a group
    * There should be no more recent records in the full release with the same
      id field
    * All UK non-metadata relationships should be present in RF1 with the same
      id
    * All International non-metadata relationships should be present in RF1
    * All defining and additional relationships in RF1 should be present in RF2

    Rules Specific to Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Relationship triples should be unique within a group at any given time
    * Fields specified as immutable may not be changed
    * Record should not predate its referenced module id
    * Record should not predate its referenced source id
    * Record should not predate its referenced destination id
    * Record should not predate its referenced typeid id
    * Record should not predate its referenced characteristic type id
    * Record should not predate its referenced modifier id
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release.
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_sct_rel_file(
            root_dir, date, verbose=verbose, release=release
    ):
        rows.append(
            dict(
                relationship_id=row['id'],
                effective_time=row['effectiveTime'],
                is_active=row['active'],
                module_id=row['moduleId'],
                source_id=row['sourceId'],
                destination_id=row['destinationId'],
                relationship_group=row['relationshipGroup'],
                type_id=row['typeId'],
                characteristic_type_id=row['characteristicTypeId'],
                modifier_id=row['modifierId']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.SctRelationship,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.SctRelationship,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_stated_relationships(session, root_dir, date, verbose=False,
                                release=common.FULL_RELEASE):
    """This is the stated relationships between different concepts.

    Currently this is empty, although I am not sure what the conceptual
    difference is between this and relationships.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implement all of these:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be between 100000 and 999999999999999999
    * Partition digits of id field should reflect component source and
      identifier type
    * Check digit in id field should be correct according to Verhoeff function
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1
    * Module id should be present in snapshot concept table
    * Module id should be a subtype of the appropriate metadata concept
    * id, effectivetime and module id should be unique
    * No field should be null
    * Source id should exist in snapshot concept table
    * Destination id should exist in snapshot concept table
    * Relationship group should be a non-negative integer
    * Characteristic type id should be a subtype of the appropriate metadata
      concept
    * Modifier id should be a subtype of the appropriate metadata concept
    * Relationships may not be reflexive
    * Grouped relationships should be defining but not hierarchical
    * Extension relationships may not define international concepts

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * Relationship triple should be unique within a group
    * Type id should exist in snapshot concept table
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Root concept should exist as a supertype (only) in the hierarchy
    * Relationship groups should contain more than one relationship
    * Ungrouped relationships may not also occur within a group
    * There should be no more recent records in the full release with the same
      id field
    * All non-metadata relationships should be present in RF1

    Rules Specific to Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Relationship triples should be unique within a group at any given time
    * Fields specified as immutable may not be changed
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_sct_rel_file(
            root_dir, date, verbose=verbose, stated_rel=True, release=release
    ):
        rows.append(
            dict(
                relationship_id=row['id'],
                effective_time=row['effectiveTime'],
                is_active=row['active'],
                module_id=row['moduleId'],
                source_id=row['sourceId'],
                destination_id=row['destinationId'],
                relationship_group=row['relationshipGroup'],
                type_id=row['typeId'],
                characteristic_type_id=row['characteristicTypeId'],
                modifier_id=row['modifierId']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.SctStatedRelationship,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.SctStatedRelationship,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_description_lookup(session, root_dir, date, verbose=False,
                              release=common.FULL_RELEASE):
    """This imports a description ID lookup table, with unique IDs in it.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    """
    idx = 0
    commit = 100000
    # prev_id = None
    rows = []
    seen_id = set()
    for row in common.read_sct_desc_file(
            root_dir, date, verbose=verbose, release=release
    ):
        cur_id = row['id']
        if cur_id in seen_id:
            continue

        rows.append(
            dict(
                description_id=row['id']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.DescriptionId,
                rows
            )
            rows = []
            idx = 0
        seen_id.add(cur_id)
    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.DescriptionId,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_descriptions(session, root_dir, date, verbose=False,
                        release=common.FULL_RELEASE):
    """This is the concepts mapped to their text descriptions.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implemented all of these:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be between 100000 and 999999999999999999
    * Partition digits of id field should reflect component source and
      identifier type
    * Check digit in id field should be correct according to Verhoeff function
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1
    * Module id should be present in snapshot concept table
    * Module id should be a subtype of the appropriate metadata concept
    * id, effectivetime and moduleid should be unique
    * No field should be null
    * Concept id should exist in snapshot concept table
    * Language code must be a recognised type
    * Type id should exist in snapshot concept table
    * Type id should be a subtype of the appropriate metadata concept
    * Terms should not contain tabs
    * Terms should not contain linefeeds
    * Terms should not contain carriage returns
    * Case significance id should exist in snapshot concept table
    * Case significance id should be a subtype of the appropriate metadata
      concept

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * FSNs of active concepts should terminate with a semantic tag
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Concept id must exist in snapshot concept table
    * A concept can only have one FSN in a particular language
    * Active FSNs must be unique for a given language
    * A concept's synonyms should be unique within a given language
    * There should be no more recent records in the full release with the same
      id field
    * All non-metadata IDs should be present in RF1
    * All non-metadata terms should be present in RF1

    Rules Specific to Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Fields specified as immutable may not be changed
    * Record should not predate its referenced module id
    * Record should not predate its referenced concept id
    * Record should not predate its referenced type id
    * Record should not predate its referenced case significance id
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_sct_desc_file(
            root_dir, date, verbose=verbose, release=release
    ):
        rows.append(
            dict(
                description_id=row['id'],
                concept_id=row['conceptId'],
                effective_time=row['effectiveTime'],
                module_id=row['moduleId'],
                type_id=row['typeId'],
                case_significance_id=row['caseSignificanceId'],
                is_active=row['active'],
                term=row['term'],
                language_code=row['languageCode']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.SctDescription,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.SctDescription,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_concept_lookup(session, root_dir, date, verbose=False,
                          release=common.FULL_RELEASE):
    """This imports a concept lookup table, with unique concept IDs in it.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    """
    idx = 0
    commit = 100000
    # prev_id = None
    rows = []

    # This is a quick hack, I should really external merge sort them
    seen_id = set()
    for row in common.read_sct_concept_file(
            root_dir, date, verbose=verbose, release=release
    ):
        cur_id = row['id']
        if cur_id in seen_id:
            continue

        rows.append(
            dict(
                concept_id=row['id']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.ConceptId,
                rows
            )
            rows = []
            idx = 0
        seen_id.add(cur_id)

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.ConceptId,
            rows
        )
    session.commit()
    return len(seen_id)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def import_concepts(session, root_dir, date, verbose=False,
                    release=common.FULL_RELEASE):
    """This is the concepts mapped back to their module ID and definition
    status IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object bound to the SnomedCT database.
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.

    Notes
    -----
    From the documentation, I have not implemented all of these:

    Rules Common to Delta, Snapshot and Full Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be between 100000 and 999999999999999999
    * Partition digits of id field should reflect component source and
      identifier type
    * Check digit in id field should be correct according to Verhoeff function
    * Effective time should be between earliest release and present date
    * Active field should be 0 or 1  (implemented)
    * Module id should be present in snapshot concept table  (implemented)
    * Module id should be a subtype of the appropriate metadata concept
    * id, effective time and module id should be unique (implemented)
    * No field should be null  (implemented)
    * Definition status id should exist in snapshot concept table
    * Definition status id should be a subtype of the appropriate metadata
      concept

    Rules Common to Delta and Snapshot Releases
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id field should be unique
    * An identical record must be present in the full release

    Rules Specific to Delta Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Effective time should be the date of the latest release
    * An identical record must be present in the snapshot release

    Rules Specific to Snapshot Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * Each concept requires an active FSN
    * Each concept requires an active synonym
    * Each active concept except the root requires a supertype
    * There should be no more recent records in the full release with the same
      id field
    * All non-metadata IDs should be present in RF1
    * Active flag should reflect RF1 status

    Rules Specific to Full Release
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    * id and effective time should be unique
    * Record should not predate its referenced module id
    * Record should not predate its referenced definition status
    * Records with the latest release date should be present in the delta
      release
    * The latest record with a given id should be present in the snapshot
      release
    * All previous records must be present in the full release
    """
    idx = 0
    commit = 100000
    rows = []
    for row in common.read_sct_concept_file(
            root_dir, date, verbose=verbose, release=release
    ):
        rows.append(
            dict(
                concept_id=row['id'],
                effective_time=row['effectiveTime'],
                module_id=row['moduleId'],
                definition_status_id=row['definitionStatusId'],
                is_active=row['active']
            )
        )
        idx += 1
        if idx == commit:
            session.bulk_insert_mappings(
                orm.SctConcept,
                rows
            )
            rows = []
            idx = 0

    if len(rows) > 0:
        session.bulk_insert_mappings(
            orm.SctConcept,
            rows
        )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_data(inzip, out_dir):
    """Extract all the files from the ZIP archive.

    This is not used anymore as there are multiple distributions in each
    archive.

    Parameters
    ----------
    inzip : `str`
        The input ZIP archive
    out_dir : `str`
        The directory to extract the ZIP archive into

    Returns
    -------
    extracted_files : `list` or `str`
        the paths to the files extracted from the archive.
    """
    with ZipFile(inzip, 'r') as zip:
        root_dirs = set()
        for i in zip.namelist():
            root_dirs.add(i.split(os.path.sep)[0])
        if len(root_dirs) != 1:
            raise ValueError(
                "expected only a single root directory in zip archive:"
                f" {len(root_dirs)}"
            )
        root_dir = os.path.join(
            os.path.dirname(
                os.path.realpath(
                    os.path.expanduser(inzip)
                )
            ), list(root_dirs)[0]
        )
        print(root_dir)
        # extract all files to another directory
        zip.extractall(out_dir)
    return root_dir


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_zip_file_data(zip_file):
    """Extract the version and file type information from the file name of the
    zip file.

    This is not used anymore as there are multiple distributions in each
    archive.

    Parameters
    ----------
    zip_file : `str`
        The path to the zip file.

    Returns
    -------
    file_info : `FileInfo`
        A ``FileInfo`` named tuple with the ``type`` attribute ``zip`` or
        ``bonus_zip``.

    Notes
    -----
    The expectation is that the zip file base name will have the structure
    below:

    * monolith version - ``uk_sct2mo_36.1.0_20230510000001Z.zip``
    * drug version - ``uk_sct2dr_36.1.0_20230510000001Z.zip``
    """
    fn_regex = re.compile(
        r"""
        uk_sct2(?P<EDITION>[\w\d]+)_ # prefix
        (?P<MAJOR>\d+)\. # The major version number and delimiter
        (?P<MINOR>\d+)\. # The minor version number
        (?P<PATCH>\d+)_ # The patch version number
        (?P<DATE>\d{8})\d+Z\.zip # Date and file extension
        """,
        re.VERBOSE
    )

    m = fn_regex.match(os.path.basename(zip_file))

    try:
        date = common.parse_date(m.group("DATE"))
        version = "{0}.{1}.{2}".format(
            m.group("MAJOR"), m.group("MINOR"), m.group("PATCH")
        )
        edition = common.EDITION_MAPPER[m.group('EDITION')]
    except KeyError as e:
        raise KeyError(f"unknown edition: {m.group('EDITION')}") from e
    except AttributeError as e:
        raise KeyError(f"can't detect file info: {zip_file}") from e

    fi = common.FileInfo(path=zip_file, type='zip', version=version,
                         date=date, edition=edition, release=None)
    return fi


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
