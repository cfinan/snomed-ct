"""Common functions and classes
"""
from datetime import date
from snomed_ct import (
    orm as orm
)
from snomed_ct.example_data import examples
from datetime import datetime
from tqdm import tqdm
from collections import namedtuple
from glob import glob
import sys
import os
import csv
import re
import json
# import pprint as pp


EPOCH_DATE = date(2000, 1, 1)

try:
    _DEFAULT_CONFIG = os.path.join(os.environ['HOME'], '.db_conn.cnf')
    """The default fallback path for the config file, root of the home
    directory (`str`)
    """
except KeyError:
    # HOME environment variable does not exist for some reason
    _DEFAULT_CONFIG = None

SNAPSHOT_RELEASE = "Snapshot"
"""The name for the snapshot release (`str`)
"""
DELTA_RELEASE = "Delta"
"""The name for the delta release (`str`)
"""
FULL_RELEASE = "Full"
"""The name for the full release (`str`)
"""
MUNGE_RELEASE = "Munge"
"""The name for my merged release release (`str`)
"""
ALLOWED_RELEASES = [
    SNAPSHOT_RELEASE, DELTA_RELEASE, FULL_RELEASE, MUNGE_RELEASE
]
"""The allowed SnomedCT releases (`list` of `str`)
"""

_DEFAULT_SECTION = 'snomed_ct'
"""The default section name in an uni config file that contains SQLAlchemy
connection parameters (`str`)
"""

_DEFAULT_PREFIX = 'snomed_ct.'
"""The default prefix name in an ini config section each SQLAlchemy connection
parameter should be prefixed with this (`str`)
"""

RELEASE_INFO_FILE = "release_package_information.json"
"""The basename for the release information file (`str`)
"""
TERMINOLOGY_RELATIVE_PATH = os.path.join('{release}', 'Terminology')
"""The relative path to the sct2_* files, this is relative from the unzipped
root path (`str`)
"""
REFSET_CONTENT_RELATIVE_PATH = os.path.join('{release}', 'Refset', 'Content')
"""The relative path to the der2_* content files, this is relative from
 the unzipped root path (`str`)
"""


CONCEPT_FILE = os.path.join(
    TERMINOLOGY_RELATIVE_PATH,
    'sct2_Concept_*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for a template concept file (`str`)
"""
DESCRIPTION_FILE = os.path.join(
    TERMINOLOGY_RELATIVE_PATH,
    'sct2_Description_*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for a template concept file (`str`)
"""
RELATIONSHIP_FILE = os.path.join(
    TERMINOLOGY_RELATIVE_PATH,
    'sct2_Relationship_*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for a template concept file (`str`)
"""
STATED_RELATIONSHIP_FILE = os.path.join(
    TERMINOLOGY_RELATIVE_PATH,
    'sct2_StatedRelationship_*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for a template concept file (`str`)
"""
TEXT_DEFINITION_FILE = os.path.join(
    TERMINOLOGY_RELATIVE_PATH,
    'sct2_TextDefinition_*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for the text definition file (`str`)
"""
SIMPLE_REFSET_FILE = os.path.join(
    REFSET_CONTENT_RELATIVE_PATH,
    'der2_Refset_Simple*{release}{text}_[GI][BN]*_{date}.txt'
)
"""A relative path (including dirs) for the simple refset file (`str`)
"""

DELIMITER = "\t"
"""The delimiter of the input files (`str`)
"""

# https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
DATE_FORMAT = "%Y%m%d"
"""The date format in the SnomedCT release (`str`)
"""

DirInfo = namedtuple(
    'DirInfo',
    [
        'path', 'modules', 'edition', 'release', 'version',
        'date', 'dir_date', 'delta_from_date', 'delta_to_date', 'lang_refsets'
    ]
)
"""Will hold information on the ZIP archive and the XML files extracted from
the zip file (`DirInfo`)
"""

# Increase the CSV field size limit
csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class SnomedCodeParser(object):
    """Parse Snomed codes into their component parts.
    """
    CODE_TYPE_CONCEPT = 'CONCEPT'
    """A concept Snomed code type (`str`)
    """
    CODE_TYPE_DESC = 'DESCRIPTION'
    """A description Snomed code type (`str`)
    """
    CODE_TYPE_REL = 'RELATIONSHIP'
    """A relationship Snomed code type (`str`)
    """
    CODE_TYPE_UNKNOWN = 'UNKNOWN'
    """An unknown Snomed code type (`str`)
    """

    NAMESPACE = examples.get_data('snomed_namespace')
    """Mappings between the snomed namespace IDs (`int`) and organisation
    names (`str`) (`dict`)
    """
    PARTITION_ID = {
        '00': (False, CODE_TYPE_CONCEPT),
        '01': (False, CODE_TYPE_DESC),
        '02': (False, CODE_TYPE_REL),
        '10': (True, CODE_TYPE_CONCEPT),
        '11': (True, CODE_TYPE_DESC),
        '12': (True, CODE_TYPE_REL),
    }
    """Mappings between partition IDs (`int`) and tuples of
    (``has_namespace`` (`bool`), ``code_type`` (str)) (`dict`)
    """

    SnomedCode = namedtuple(
        'SnomedCode', [
            'code', 'verhoeff_valid', 'check_digit', 'partition_id',
            'has_namespace', 'code_type', 'partition_valid',
            'namespace_id', 'namespace_valid', 'organisation'
        ]
    )
    """A representation of a parsed Snomed code (`namedtuple`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def parse(cls, snomed_code):
        """Parse and perform validation on SnomedCT codes.

        Parameters
        ----------
        snomed_code : `int`
            The Snomed code to parse. Can be `NoneType` or negative, which are
            interpreted as invalid codes.

        Returns
        -------
        parsed_code : `snomed_ct.common.SnomedCodeParser.SnomedCode`
            A parsed code, is the given snomed code is not an int or can't be
            cast into an int then an invalid parsed_code is returned.
        """
        try:
            snomed_code = int(snomed_code)

            if snomed_code <= 0:
                raise ValueError("bad_code")
        except Exception:
            # return a blank record
            return cls.SnomedCode(
                code=-1,
                verhoeff_valid=False,
                check_digit=-1,
                partition_id=cls.CODE_TYPE_UNKNOWN,
                has_namespace=False,
                code_type=cls.CODE_TYPE_UNKNOWN,
                partition_valid=False,
                namespace_id=-1,
                namespace_valid=False,
                organisation=cls.CODE_TYPE_UNKNOWN
            )

        is_valid = Verhoeff.validate(snomed_code)
        str_code = str(snomed_code)
        check_digit = int(str_code[-1])
        # partition_id = str_code[slice(-4, -2)]
        partition_id = str_code[slice(-3, -1)]
        namespace_valid = False
        organisation = cls.CODE_TYPE_UNKNOWN
        try:
            has_namespace, concept_type = cls.PARTITION_ID[partition_id]
            partition_valid = True

            namespace_id = 0
            if has_namespace is True:
                namespace_id = int(str_code[slice(-10, -3)])

            try:
                organisation = cls.NAMESPACE[namespace_id]
                namespace_valid = True
            except KeyError:
                namespace_valid = False
                organisation = None
        except KeyError:
            has_namespace = False
            partition_valid = False
            concept_type = cls.CODE_TYPE_UNKNOWN
            namespace_id = -1

        return cls.SnomedCode(
            code=snomed_code,
            verhoeff_valid=is_valid,
            check_digit=check_digit,
            partition_id=partition_id,
            has_namespace=has_namespace,
            code_type=concept_type,
            partition_valid=partition_valid,
            namespace_id=namespace_id,
            namespace_valid=namespace_valid,
            organisation=organisation
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Verhoeff:
    """Calculate and verify check digits using Verhoeff's algorithm. Taken
    from: https://codereview.stackexchange.com/questions/221229/
    """

    MULTIPLICATIONS = (
        (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
        (1, 2, 3, 4, 0, 6, 7, 8, 9, 5),
        (2, 3, 4, 0, 1, 7, 8, 9, 5, 6),
        (3, 4, 0, 1, 2, 8, 9, 5, 6, 7),
        (4, 0, 1, 2, 3, 9, 5, 6, 7, 8),
        (5, 9, 8, 7, 6, 0, 4, 3, 2, 1),
        (6, 5, 9, 8, 7, 1, 0, 4, 3, 2),
        (7, 6, 5, 9, 8, 2, 1, 0, 4, 3),
        (8, 7, 6, 5, 9, 3, 2, 1, 0, 4),
        (9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
    )
    """The permutations lookup table (`tuple` of `tuple` of `int`)
    """

    INVERSES = (0, 4, 3, 2, 1, 5, 6, 7, 8, 9)
    """The permutations lookup table (`tuple` of `int`)
    """

    PERMUTATIONS = (
        (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
        (1, 5, 7, 6, 2, 8, 3, 0, 9, 4),
        (5, 8, 0, 3, 7, 9, 6, 1, 4, 2),
        (8, 9, 1, 6, 0, 4, 3, 5, 2, 7),
        (9, 4, 5, 3, 1, 2, 6, 8, 7, 0),
        (4, 2, 8, 6, 5, 7, 3, 9, 0, 1),
        (2, 7, 9, 3, 8, 0, 6, 4, 1, 5),
        (7, 0, 4, 6, 9, 1, 3, 2, 5, 8)
    )
    """The permutations lookup table (`tuple` of `tuple` of `int`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _find_check_digit(cls, digits):
        """Common check digit function.

        Parameters
        ----------
        digits : `str`
            The digits to check.

        Returns
        -------
        check_digit : `int`
            The check digit.
        """
        check_digit = 0
        for i, digit in digits:
            col_idx = cls.PERMUTATIONS[i % 8][int(digit)]
            check_digit = cls.MULTIPLICATIONS[check_digit][col_idx]
        return check_digit

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def calculate(cls, input_: str) -> str:
        """Calculate the check digit using Verhoeff's algorithm

        Parameters
        ----------
        input : `str`
            The digits to check.

        Returns
        -------
        check_digit : `int`
            The check digit."""
        input_ = str(input_)
        check_digit = cls._find_check_digit(enumerate(reversed(input_), 1))
        return str(cls.INVERSES[check_digit])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def validate(cls, input_: str) -> bool:
        """Validate the check digit using Verhoeff's algorithm"""
        input_ = str(input_)
        check_digit = cls._find_check_digit(enumerate(reversed(input_)))
        return cls.INVERSES[check_digit] == 0


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_dir_info(indir):
    """Extract the release information from the directory name and the
    ``release_package_information.json`` file in the directory.

    Parameters
    ----------
    indir : `str`
        The path to the input directory.

    Returns
    -------
    dir_info : `snomed_ct.common.DirInfo`
        A named tuple with the essential release information in it.
    """
    root_dir = os.path.realpath(os.path.expanduser(indir))
    base_dir = os.path.basename(root_dir)

    dir_match = re.match(
        r"""
        SnomedCT_(?P<EDITION>[\w\d]+)RF2_ # prefix and edition
        (?P<STABILITY>[\w\d]+)_           # stability i.e production
        (?P<DATE>\d{8})[\w\d]+Z           # Date
        """,
        base_dir,
        re.VERBOSE
    )

    try:
        edition = dir_match.group('EDITION')
        stability = dir_match.group('STABILITY')
        dir_date = parse_date(dir_match.group('DATE'))
    except AttributeError as e:
        raise ValueError(
            f"unable to parse directory name: {base_dir}"
        ) from e

    releases = []
    for i in os.listdir(root_dir):
        if i in ALLOWED_RELEASES:
            releases.append(i)

    if len(releases) == 0:
        raise ValueError(f"can't find any releases: {root_dir}")

    release_info = get_release_info(root_dir)
    date, df_date, dt_date, modules, lang_refsets = get_release_data(
        release_info
    )
    return DirInfo(
        path=root_dir,
        modules=modules,
        lang_refsets=lang_refsets,
        edition=edition,
        release=releases,
        version=stability,
        dir_date=dir_date,
        date=date,
        delta_to_date=dt_date,
        delta_from_date=df_date
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_release_data(release_info):
    """Extract the essential release data from the release info.

    Parameters
    ----------
    release_info : `dict`
        The release information from the ``release_package_information.json``
        file within the zip archive.

    Returns
    -------
    date : `datetime.datetime`
        The release date.
    delta_from_date : `datetime.datetime`
        The date where any delta releases start from.
    delta_to_date : `datetime.datetime`
        The date where any delta releases run to.
    modules : `list` of `int`
        The concept IDs of modules in the release. This could be empty.
    language_refsets : `list` of `int`
        The concept IDs of language reference sets in the release. This could
        be empty.
    """
    date = parse_date(release_info['effectiveTime'])

    try:
        delta_from_date = parse_date(release_info['deltaFromDate'])
    except KeyError:
        delta_from_date = None
    try:
        delta_to_date = parse_date(release_info['deltaToDate'])
    except KeyError:
        delta_to_date = None
    try:
        language_refsets = [
            int(i['id']) for i in release_info['languageRefsets']
        ]
    except KeyError:
        language_refsets = [
            int(i['id']) for i in release_info['humanReadableLanguageRefset']
        ]

    try:
        modules = [int(i) for i in release_info['modules'].keys()]
    except KeyError:
        modules = []

    return date, delta_from_date, delta_to_date, modules, language_refsets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_release_info(indir):
    """Open the ``release_package_information.json`` file and parse it.

    Parameters
    ----------
    indir : `str`
        The root directory of the extracted zip archive.

    Returns
    -------
    release_info : `dict`
        The release information from the ``release_package_information.json``
        file within the zip archive.
    """
    release_file = os.path.join(indir, RELEASE_INFO_FILE)

    try:
        with open(release_file, 'rt') as infile:
            return json.loads(infile.read())
    except FileNotFoundError as e:
        raise FileNotFoundError(
            f"can't find release info JSON file: {release_file}"
        ) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_file(root_dir, relative_path, date, verbose=False,
               desc="[info] importing...", unit=" rows", leave=False,
               release=FULL_RELEASE, is_text=False):
    """A simple generic file reader to handle setting up of csv.DictReader
    and verbosity.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    relative_path : `str`
        The path to the input file relative to the ``root_dir``.
    date : `datetime.datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    desc : `str`, optional, default: `[info] importing...`
        The progress description.
    unit : `str`, optional, default: ` rows`
        The progress units.
    leave : `bool`, optional, default: `False`
        Should the progress bar remain in the cmd-line after the read has
        finished.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values.
    """
    import_file_name = get_file(
        root_dir, relative_path, date, release=release, is_text=is_text
    )

    with open(import_file_name, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter=DELIMITER)

        tqdm_kwargs = dict(
            desc=desc,
            unit=unit,
            leave=leave,
            disable=not verbose
        )
        for row in tqdm(reader, **tqdm_kwargs):
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_sct_concept_file(root_dir, date, release=FULL_RELEASE,
                          verbose=False, active_bool=True, int_date=False,
                          **kwargs):
    """Read the Snomed terminology concept file.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime.datetime`
        The date for the imported version.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    active_bool : `bool`, optional, default: `True`
        Should the ``active`` filed be passed as a boolean (True) or integer
        (False).
    int_date : `bool`, optional, default: `False`
        Should the ``effectiveTime`` filed be passed as a datetime object
        (False) or integer (True).
    **kwargs
        Ignored.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values. The column values are the correct type for import.
    """
    kwargs = dict(
        verbose=verbose,
        leave=False,
        unit=" concepts",
        desc="[info] reading concepts...",
        release=release,
        is_text=False
    )

    bool_cast = int
    if active_bool is True:
        bool_cast = parse_bool

    date_cast = parse_date
    if int_date is True:
        date_cast = int

    for row in _read_file(root_dir, CONCEPT_FILE, date, **kwargs):
        row['id'] = int(row['id'])
        row['effectiveTime'] = date_cast(row['effectiveTime'])
        row['active'] = bool_cast(row['active'])
        row['moduleId'] = int(row['moduleId'])
        row['definitionStatusId'] = int(row['definitionStatusId'])
        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_sct_desc_file(root_dir, date, release=FULL_RELEASE, verbose=False,
                       active_bool=True, int_date=False, **kwargs):
    """Read the Snomed terminology description file.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime.datetime`
        The date for the imported version.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    active_bool : `bool`, optional, default: `True`
        Should the ``active`` filed be passed as a boolean (True) or integer
        (False).
    int_date : `bool`, optional, default: `False`
        Should the ``effectiveTime`` filed be passed as a datetime object
        (False) or integer (True).
    **kwargs
        Ignored.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values. The column values are the correct type for import.
    """
    kwargs = dict(
        verbose=verbose,
        leave=False,
        unit=" desc",
        desc="[info] reading descriptions...",
        release=release,
        is_text=True
    )

    bool_cast = int
    if active_bool is True:
        bool_cast = parse_bool

    date_cast = parse_date
    if int_date is True:
        date_cast = int

    for row in _read_file(root_dir, DESCRIPTION_FILE, date, **kwargs):
        row['id'] = int(row['id'])
        row['effectiveTime'] = date_cast(row['effectiveTime'])
        row['active'] = bool_cast(row['active'])
        row['moduleId'] = int(row['moduleId'])
        row['conceptId'] = int(row['conceptId'])
        row['typeId'] = int(row['typeId'])
        row['caseSignificanceId'] = int(row['caseSignificanceId'])
        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_sct_rel_file(root_dir, date, verbose=False, release=FULL_RELEASE,
                      stated_rel=False, active_bool=True, int_date=False,
                      only_active=False, **kwargs):
    """A read function for the Snomed terminology relationship/stated
    relationship file.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime.datetime`
        The date for the imported version.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    started_rel : `bool`, optional, default: `False`
        Should the stated relationships file be parsed (True) instead of the
        relationships file (False).
    active_bool : `bool`, optional, default: `True`
        Should the ``active`` filed be passed as a boolean (True) or integer
        (False).
    int_date : `bool`, optional, default: `False`
        Should the ``effectiveTime`` filed be passed as a datetime object
        (False) or integer (True).
    only_active : `bool`, optional, default: `False`:
        Only yield active relationships.
    **kwargs
        Ignored.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values. The column values are the correct type for import. The columns
        are:

        1. ``id`` (`int`)
        2. ``effectiveTime`` (`datetime`)
        3. ``active`` (`bool`)
        4. ``moduleId`` (`int`)
        5. ``sourceId`` (`int`)
        6. ``destinationId`` (`int`)
        7. ``relationshipGroup`` (`int`)
        8. ``typeId`` (`int`)
        9. ``characteristicTypeId`` (`int`)
        10. ``modifierId`` (`int`)
    """
    file_type = RELATIONSHIP_FILE
    stated = ""
    if stated_rel is True:
        file_type = STATED_RELATIONSHIP_FILE
        stated = "stated"

    kwargs = dict(
        verbose=verbose,
        leave=False,
        unit=" rel",
        desc=f"[info] reading {stated} relationships...",
        release=release,
        is_text=False
    )

    bool_cast = int
    if active_bool is True:
        bool_cast = parse_bool

    date_cast = parse_date
    if int_date is True:
        date_cast = int

    for row in _read_file(root_dir, file_type, date, **kwargs):
        row['id'] = int(row['id'])
        row['effectiveTime'] = date_cast(row['effectiveTime'])
        row['active'] = bool_cast(row['active'])
        row['moduleId'] = int(row['moduleId'])
        row['sourceId'] = int(row['sourceId'])
        row['destinationId'] = int(row['destinationId'])
        row['relationshipGroup'] = int(row['relationshipGroup'])
        row['typeId'] = int(row['typeId'])
        row['characteristicTypeId'] = int(row['characteristicTypeId'])
        row['modifierId'] = int(row['modifierId'])

        if only_active is True and row['active'] == 0:
            continue

        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_sct_text_def_file(root_dir, date, release=FULL_RELEASE,
                           verbose=False, active_bool=True, int_date=False,
                           **kwargs):
    """A read function for the Snomed terminology text definition file.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime.datetime`
        The date for the imported version.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    active_bool : `bool`, optional, default: `True`
        Should the ``active`` filed be passed as a boolean (True) or integer
        (False).
    int_date : `bool`, optional, default: `False`
        Should the ``effectiveTime`` filed be passed as a datetime object
        (False) or integer (True).
    **kwargs
        Ignored.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values. The column values are the correct type for import. The columns
        are:

        1. ``id`` (`int`)
        2. ``effectiveTime`` (`datetime`)
        3. ``active`` (`bool`)
        4. ``moduleId`` (`int`)
        5. ``sourceId`` (`int`)
        6. ``destinationId`` (`int`)
        7. ``relationshipGroup`` (`int`)
        8. ``typeId`` (`int`)
        9. ``characteristicTypeId`` (`int`)
        10. ``modifierId`` (`int`)
    """
    kwargs = dict(
        verbose=verbose,
        leave=False,
        unit=" rel",
        desc="[info] reading text definitions...",
        release=release,
        is_text=True
    )

    bool_cast = int
    if active_bool is True:
        bool_cast = parse_bool

    date_cast = parse_date
    if int_date is True:
        date_cast = int

    for row in _read_file(root_dir, TEXT_DEFINITION_FILE, date, **kwargs):
        row['id'] = int(row['id'])
        row['effectiveTime'] = date_cast(row['effectiveTime'])
        row['active'] = bool_cast(row['active'])
        row['moduleId'] = int(row['moduleId'])
        row['conceptId'] = int(row['conceptId'])
        row['typeId'] = int(row['typeId'])
        row['caseSignificanceId'] = int(row['caseSignificanceId'])
        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_der_simple_refset_file(root_dir, date, release=FULL_RELEASE,
                                verbose=False, active_bool=True,
                                int_date=False, **kwargs):
    """A read function for the Snomed simple refset file:
    ``der2_Refset_SimpleMONOSnapshot_GB_<date>.txt``

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    date : `datetime.datetime`
        The date for the imported version.
    release : `str`, optional, default: `Full`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    verbose : `bool`, optional, default: `False`
        Should the file reading/load progress be output.
    active_bool : `bool`, optional, default: `True`
        Should the ``active`` filed be passed as a boolean (True) or integer
        (False).
    int_date : `bool`, optional, default: `False`
        Should the ``effectiveTime`` filed be passed as a datetime object
        (False) or integer (True).
    **kwargs
        Ignored.

    Yields
    ------
    row : `dict`
        An input row. The keys are column names and the values are column
        values. The column values are the correct type for import. The columns
        are:

        1. ``id`` (`str`)
        2. ``effectiveTime`` (`datetime`)
        3. ``active`` (`bool`)
        4. ``moduleId`` (`int`)
        5. ``refsetId`` (`int`)
        6. ``referencedComponentId`` (`int`)
    """
    kwargs = dict(
        verbose=verbose,
        leave=False,
        unit=" refsets",
        desc="[info] reading simple refset...",
        release=release,
    )

    bool_cast = int
    if active_bool is True:
        bool_cast = parse_bool

    date_cast = parse_date
    if int_date is True:
        date_cast = int

    for row in _read_file(root_dir, SIMPLE_REFSET_FILE, date, **kwargs):
        row['effectiveTime'] = date_cast(row['effectiveTime'])
        row['active'] = bool_cast(row['active'])
        row['moduleId'] = int(row['moduleId'])
        row['refsetId'] = int(row['refsetId'])
        row['referencedComponentId'] = int(row['referencedComponentId'])
        yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_file(root_dir, relative_path, date, release=FULL_RELEASE,
             is_text=False):
    """Return the expected full path to a file.

    Parameters
    ----------
    root_dir : `str`
        The source directory for the input files, exactly as extracted from the
        zip archive.
    relative_path : `str`
        the relative path to the file, including any sub-directories.
    date : `datetime.datetime`
        The date for the imported version.
    release : `str`, optional, default: `snapshot`
        The release of the SnomedCT that is being imported. Either, `snapshot`,
        `full`, `delta`.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.

    Returns
    -------
    full_path : `str`
        The expected full path to the file.
    """
    text_date = date.strftime("%Y%m%d")

    text = ""
    if is_text is True:
        text = "-en"

    # Make sure the release is formatted correctly
    release = release.title()

    glob_file = os.path.join(
        root_dir, relative_path.format(
            date=text_date,
            release=release,
            text=text
        )
    )

    files = glob(glob_file)

    if len(files) > 1:
        raise ValueError(f"too many files: {','.join(files)}")
    elif len(files) == 0:
        raise ValueError(f"can't identify file from: {glob_file}")
    return files[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(text_date):
    """Parse a text date from the SnomedCT files into a datetime object foreign
    loading into the database.

    Parameters
    ----------
    text_date : `str`
        The date to parse. i.e. 20230603.

    Returns
    -------
    parsed_date : `datetime.datetime`
        The parsed date.
    """
    return datetime.strptime(text_date, DATE_FORMAT)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(data):
    """Parse a 1/0 into a Python boolean True/False value.

    Parameters
    ----------
    data : `str` or `int`
        The data to parse.

    Returns
    -------
    bool_data : `bool`
        The boolean type of the data.
    """
    return bool(int(data))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_tables(session):
    """Ensure all the tables are created.

    Parameters
    ----------
    session : `sqlalchemy.session`
        The session object, must return the engine when `get_bind()` is called
        on it.
    """
    orm.Base.metadata.create_all(
        session.get_bind(), checkfirst=True
    )
