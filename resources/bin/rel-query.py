#!/usr/bin/env python
"""Small test script to query relationships from SnomedCT
"""
from snomed_ct import orm, query
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
import pprint as pp


db_path = os.path.join("data", "snomedct", "snomed-ct-36-munge.db")
# print(db_path)
engine = create_engine(f"sqlite:////{db_path}")

# create a configured "Session" class
Session = sessionmaker(bind=engine)

# create a Session
session = Session()

cid = 32629811000001101
rel_trees = query.get_source_releationships(
    session, cid, distance=1)

all_extracts = []
for idx, t in enumerate(rel_trees):
    print(f"TREE: {idx}")
    # print(t)
    extree = query.parse_relationship_tree(t)
    all_extracts.append(all_extracts)
    source_str = ""
    for idx2, i in enumerate(extree):
        source_str = (
            f"{source_str}"
            f"({i['source']['concept_id']}|{i['source']['term']})"
            f"->[{i['rel_type']['term']}|{i['char_type']['term']}|{i['modifier']['term']}]"
        )
        try:
            extree[idx2+1]
            source_str = source_str + "->"
        except IndexError:
            source_str = (
                f"{source_str}->"
                f"({i['destination']['concept_id']}|{i['destination']['term']})"
            )
        # pp.pprint(i)
    print(source_str)
