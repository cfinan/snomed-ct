#!/usr/bin/env python
"""Build a functional SnomedCT relational database with only a few codes in it.
 This is for demo purposes.
"""
from snomed_ct import (
    __version__,
    __name__ as pkg_name,
    common as common,
    orm as orm,
)
from sqlalchemy_config import config as cfg
from pyaddons import log
# from sqlalchemy import and_, or_
from sqlalchemy.orm.exc import NoResultFound
from tqdm import tqdm
from random import randrange
import argparse
import os
import sys
import csv
# import pprint as pp


_PROG_NAME = 'create-toy-database'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``snmed_ct.build.build_snomed_ct``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    try:
        # Delete any previous versions
        os.unlink(args.output_db)
    except FileNotFoundError:
        pass

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the database
    insm = cfg.get_sessionmaker(
        None, common._DEFAULT_PREFIX, url_arg=args.input_db,
        config_arg=None, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )
    outsm = cfg.get_sessionmaker(
        None, common._DEFAULT_PREFIX, url_arg=args.output_db,
        config_arg=None, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    insession = insm()
    outsession = outsm()

    try:
        # Make sure the database exists (or does not exist)
        cfg.create_db(outsm, exists=False)
        common.create_tables(outsession)

        # Build the SnomedCT toy database
        build_toy_db(insession, outsession, nrows=args.nrows,
                     verbose=args.verbose, core_dir=args.core_dir)
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        insession.close()
        outsession.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'input_db',
        type=str,
        help="The input SQLite database."
    )
    parser.add_argument(
        'output_db',
        type=str,
        help="The output SQLite database."
    )
    parser.add_argument(
        '--core-dir',
        type=str,
        default=os.environ['HOME'],
        help="The directory to place the core description files. Defaults to"
        " the root of the home directory"
    )
    parser.add_argument(
        '-n', '--nrows',
        type=int, default=100,
        help="The number of concepts to use in addition to the module/type"
        " concepts"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.core_dir = os.path.realpath(os.path.expanduser(args.core_dir))
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_toy_db(insession, outsession, nrows=100, verbose=False,
                 core_dir=None):
    """Add a DM+D database version to the prescription database.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    outsession : `sqlalchemy.Session`
        The session object attached to the toy snomed CT database.
    nrows : `str`
        The number of concepts in addition to the core concepts that should
        be sampled from the database".
    verbose : `bool`, optional, default: `False`
        Show progress of the dict file parsing.
    core_dir : `str`, optional, default: `NoneType`
        A directory to extract the descriptions of the core concepts to.
    """
    core_dir = core_dir or os.environ['HOME']
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    module_id_classes = [
        orm.SctConcept,
        orm.SctDescription,
        orm.SctRelationship,
        orm.SctStatedRelationship,
        orm.SctTextDef
    ]

    module_ids = set([])
    for i in module_id_classes:
        module_ids.update(get_unique_id(insession, i, 'module_id'))
    logger.info(f"#unique module_id: {len(module_ids)}")
    write_core_description(insession, module_ids, core_dir, 'module-ids.txt',
                           'module_id')

    type_id_classes = [
        orm.SctDescription,
        orm.SctRelationship,
        orm.SctStatedRelationship,
        orm.SctTextDef
    ]

    type_ids = set([])
    for i in type_id_classes:
        type_ids.update(get_unique_id(insession, i, 'type_id'))
    logger.info(f"#unique type_id: {len(type_ids)}")
    write_core_description(insession, type_ids, core_dir, 'type-ids.txt',
                           'type_id')

    # Case significance
    case_signif_classes = [
        orm.SctDescription,
        orm.SctTextDef
    ]

    case_signif_ids = set([])
    for i in case_signif_classes:
        case_signif_ids.update(
            get_unique_id(insession, i, 'case_significance_id'))

    logger.info(f"#unique case_significance_ids: {len(case_signif_ids)}")
    write_core_description(insession, case_signif_ids, core_dir,
                           'case-signif-ids.txt', 'case_significance_id')

    char_type_classes = [
        orm.SctRelationship,
        orm.SctStatedRelationship,
    ]
    char_type_ids = set([])
    for i in char_type_classes:
        char_type_ids.update(
            get_unique_id(insession, i, 'characteristic_type_id')
        )
    logger.info(f"#unique characteristic_type_id: {len(char_type_ids)}")
    write_core_description(insession, char_type_ids, core_dir,
                           'char-type-ids.txt', 'characteristic_type_id')

    def_status_ids = set(
        get_unique_id(insession, orm.SctConcept, 'definition_status_id')
    )
    logger.info(f"#unique definition_status_id: {len(def_status_ids)}")
    write_core_description(insession, def_status_ids, core_dir,
                           'def-status-ids.txt', 'definition_status_id')

    all_ids = set()
    for i in [module_ids, type_ids, case_signif_ids, char_type_ids,
              def_status_ids]:
        for j in i:
            all_ids.add(j)

    logger.info(f"#unique core IDs: {len(all_ids)}")
    get_random_concepts(insession, nrows, all_ids, verbose=verbose)
    logger.info(f"#total IDs: {len(all_ids)}")

    tqdm_kwargs = dict(
        desc="[info] picking IDs", unit=" ID", disable=not prog_verbose
    )
    # print(all_ids)
    for i in tqdm(all_ids, **tqdm_kwargs):
        # print(f"**** CORE {i} ****")
        pick_id(insession, outsession, i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_core_description(session, concept_ids, core_dir, filename,
                           core_type):
    """Write the descriptions for the core concepts to file.

    Parameters
    ----------
    concept_ids : `list` of `int`
        The concept IDs to lookup.
    core_dir : `str`
        The path to the write directory.
    filename : `str`
        The basename of the core file to write.
    """
    with open(os.path.join(core_dir, filename), 'wt') as outfile:
        writer = csv.writer(outfile, delimiter="\t", lineterminator=os.linesep)
        writer.writerow(['column', 'name', 'data_type', 'description'])

        for idx, i in enumerate(sorted(concept_ids), 1):
            desc = session.query(
                orm.SctTextDef.term
            ).filter(
                orm.SctTextDef.concept_id == i
            ).all()

            if len(desc) == 0:
                desc = session.query(
                    orm.SctDescription.term
                ).filter(
                    orm.SctDescription.concept_id == i
                ).all()
            writer.writerow(
                [idx, i, core_type, desc[0].term]
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def pick_id(insession, outsession, concept_id):
    """Query and write the data relating to a concept ID.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    outsession : `sqlalchemy.Session`
        The session object attached to the toy snomed CT database.
    concept_id : `int`
        The concept identifier to query.
    """
    # print(f"Picking: {concept_id}")
    try:
        # Is it already in there:
        outsession.query(orm.ConceptId).filter(
            orm.ConceptId.concept_id == concept_id
        ).one()
        # print(f"Got: {concept_id}")
        return
    except NoResultFound:
        # print(f"Parsing: {concept_id}")
        pass

    root_concept = insession.query(
        orm.ConceptId
    ).filter(
        orm.ConceptId.concept_id == concept_id
    ).one()

    # Not in there so we start to add
    cid = orm.ConceptId(concept_id=concept_id)
    outsession.add(cid)
    outsession.commit()

    for i in root_concept.concept:
        for j in ['module_id', 'definition_status_id']:
            pick_id(insession, outsession, getattr(i, j))
        outsession.add(
            orm.SctConcept(
                concept_pk=i.concept_pk,
                concept_id=i.concept_id,
                effective_time=i.effective_time,
                is_active=i.is_active,
                module_id=i.module_id,
                definition_status_id=i.definition_status_id
            )
        )
        outsession.commit()

    # Add the concept descriptions
    add_text(
        insession, outsession, root_concept.description, orm.DescriptionId,
        orm.SctDescription, 'description_id', 'desc_pk'
    )

    # Add any full text descriptions
    add_text(
        insession, outsession, root_concept.text_def, orm.TextDefId,
        orm.SctTextDef, 'text_def_id', 'td_pk'
    )

    # Add any relationships
    add_relationship(
        insession, outsession, root_concept.rel_source, orm.SctRelationship,
    )
    add_relationship(
        insession, outsession, root_concept.rel_destination,
        orm.SctRelationship,
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_relationship(insession, outsession, rel_list, update_class):
    """Traverse relationships and add to the toy database.

    This only adds a single relationship from all of those identified,
    otherwise the whole database is copied.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    outsession : `sqlalchemy.Session`
        The session object attached to the toy snomed CT database.
    rel_list : `list` of `snomed_ct.orm.SctRelationship`
        The relationships linked to the concept, only the first is used.
    update_class : `class` of `snomed_ct.orm.SctRelationship` or \
    `snomed_ct.orm.SctStatedRelationship`
        The ORM class to query and update.
    """
    for i in rel_list:
        try:
            # Is it already in there:
            outsession.query(orm.RelationshipId).filter(
                orm.RelationshipId.relationship_id == i.relationship_id
            ).one()
        except NoResultFound:
            outsession.add(
                orm.RelationshipId(
                    relationship_id=i.relationship_id
                )
            )
            outsession.commit()

        try:
            outsession.query(update_class).filter(
                update_class.rel_pk == i.rel_pk
            ).one()
            return
        except NoResultFound:
            for j in ['module_id', 'source_id', 'destination_id', 'type_id',
                      'characteristic_type_id', 'modifier_id']:
                pick_id(insession, outsession, getattr(i, j))

                try:
                    outsession.query(update_class).filter(
                        update_class.rel_pk == i.rel_pk
                    ).one()
                    return
                except NoResultFound:
                    outsession.add(
                        update_class(
                            rel_pk=i.rel_pk,
                            relationship_id=i.relationship_id,
                            effective_time=i.effective_time,
                            is_active=i.is_active,
                            module_id=i.module_id,
                            source_id=i.source_id,
                            destination_id=i.destination_id,
                            relationship_group=i.relationship_group,
                            type_id=i.type_id,
                            characteristic_type_id=i.characteristic_type_id,
                            modifier_id=i.modifier_id
                        )
                    )

                    outsession.commit()
        break


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_text(insession, outsession, text_list, lookup_class, update_class,
             id_field, pk_field):
    """Add text descriptions to the toy database.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    outsession : `sqlalchemy.Session`
        The session object attached to the toy snomed CT database.
    text_list : `list` of `snomed_ct.orm.SctDescription` or \
    `snomed_ct.orm.SctTextDef`
        The text descriptions linked to the concept.
    lookup_class : `class` of `snomed_ct.orm.DescriptionId` or \
    `snomed_ct.orm.TextdefId`
        The ORM class of the lookup table referenced by ``update_class``.
    update_class : `class` of `snomed_ct.orm.SctDescription` or \
    `snomed_ct.orm.SctTextDef`
        The ORM class to query and update.
    id_field : `str`
        The name of the id field in the respective ``lookup_class`` and
        ``update_class``.
    pk_field : `str`
        The name of the primary key field in the respective ``update_class``.
    """
    for i in text_list:
        try:
            # Is it already in there:
            outsession.query(lookup_class).filter(
                getattr(update_class, id_field) == getattr(i, id_field)
            ).one()
        except NoResultFound:
            kwargs = {id_field: getattr(i, id_field)}
            outsession.add(lookup_class(**kwargs))
            outsession.commit()

        for j in ['module_id', 'type_id', 'case_significance_id']:
            pick_id(insession, outsession, getattr(i, j))

        kwargs = {
            id_field: getattr(i, id_field),
            pk_field: getattr(i, pk_field)
        }
        outsession.add(
            update_class(
                effective_time=i.effective_time,
                is_active=i.is_active,
                module_id=i.module_id,
                concept_id=i.concept_id,
                language_code=i.language_code,
                type_id=i.type_id,
                term=i.term,
                case_significance_id=i.case_significance_id,
                **kwargs
            )
        )
        outsession.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_unique_id(insession, orm_class, field):
    """Get the unique IDs that occur in a field in a table.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    orm_class : `snomed_ct.orm.Base`
        A snomed_ct orm class definiton containing the ``field``.
    field `str`
        The field to get the unique concept IDs from.

    Returns
    -------
    unique_ids : `list` of `int`
        Unique concept identifiers in the table/field.
    """
    q = insession.query(
        getattr(orm_class, field)
    ).group_by(
        getattr(orm_class, field)
    )
    return [getattr(i, field) for i in q.all()]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_random_concepts(insession, nrows, core_ids, verbose=False):
    """Get a selection of random concepts to include in the toy database.

    Parameters
    ----------
    insession : `sqlalchemy.Session`
        The session object attached to the full snomed CT database.
    nrows : `int`
        The number of random concept IDs to get.
    core_ids : `list` of `int`
        A list of Ids we have already got so we do not pick > once.
    verbose : `bool`, optional, default: `False`
        Log how many concepts were selected.

    Returns
    -------
    unique_ids : `list` of `int`
        Unique concept identifiers in the table/field.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    concepts = [
        i.concept_id for i in insession.query(orm.ConceptId.concept_id).all()
    ]
    logger.info(f"#unique concept IDs: {len(concepts)}")

    npicked = 0
    while npicked < nrows:
        new_id = concepts[randrange(len(concepts))]
        if (new_id, False) not in core_ids and (new_id, True) not in core_ids:
            core_ids.add(new_id)
            npicked += 1


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
