"""Some predefined queries.
"""
from snomed_ct import orm
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_relationship_tree(tree):
    """Parse a relationship tree into human readable terms.

    Parameters
    ----------
    tree : `list` of `snomed_ct.orm.SctRelationship`
        A list of relationships representing a linear path from the source to
        the end.

    Returns
    -------
    extracted_rels : `list` of `dict`
        Each dict will have the keys: ``source``, ``destination``, ``module``,
        ``rel_type``, ``char_type``, ``modifier``. The values for these are
        dicts with ``concept_id`` and ``term`` keys.
    """
    extracted_tree = []
    for i in tree:
        tree_extract = dict()
        for j in ['source', 'destination', 'module', 'rel_type', 'char_type',
                  'modifier']:
            concept_j = getattr(i, j)
            tree_extract[j] = dict()

            try:
                tree_extract[j]['concept_id'] = concept_j.concept_id
                descs = sorted(
                    concept_j.description,
                    key=lambda x: x.effective_time
                )
                descs = sorted(
                    descs, key=lambda x: x.type_id
                )
                descs = sorted(
                    descs, key=lambda x: x.is_active,
                    reverse=True
                )
                tree_extract[j]['term'] = descs[0].term
            except AttributeError:
                tree_extract[j]['term'] = ""
        extracted_tree.append(tree_extract)
    return extracted_tree


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_source_releationships(session, concept_id, distance=0):
    """Get all the relationships for which the concept is a source.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session to query for the relationships.
    concept_id : `int`
        The snomed concept identifier.
    distance : `int`, optional, default: `0`
        The number of iterations away from the source of the relationship that
        you want to travel, use -1 to go to the end.

    Returns
    -------
    trees : `list` of `list` `snomed_ct.orm.SctRelationship`
        A list of relationships through the tree. Each sub-list represents a
        linear path from the source to the end.
    """
    q = session.query(
        orm.SctRelationship
    ).filter(
        orm.SctRelationship.source_id == concept_id
    )

    all_trees = []
    for i in q:
        trees = _get_source_releationships(
            i, distance=distance, cur_iter=0
        )
        all_trees.extend(trees)
    return all_trees


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_source_releationships(relationship, distance=0, cur_iter=0):
    """Recursive lookup for the sources of the destination of the
    relationship.

    Parameters
    ----------
    relationship : `snomed_ct.orm.SctRelationship`
        The relationship object.
    distance : `int`, optional, default: `0`
        The number of iterations away from the source of the relationship that
        you want to travel, use -1 to go to the end.
    cur_iter : `int`, optional, default: `0`
        The current iteration that the function is on.

    Returns
    -------
    trees : `list` of `list` `snomed_ct.orm.SctRelationship`
        A list of relationships through the tree. Each sub-list represents a
        linear path from the source to the end.
    """
    trees = []
    if distance == -1 or cur_iter < distance:
        try:
            next_rels = []
            next_rels = [
                i for i in relationship.destination.rel_source
                if i.is_active is True
            ]

            if len(next_rels) == 0:
                raise AttributeError("no next source")

            cur_iter += 1
            for i in next_rels:
                next_trees = _get_source_releationships(
                    i, distance=distance, cur_iter=cur_iter
                )
                for n in next_trees:
                    trees.append([relationship] + n)
        except AttributeError:
            trees.append([relationship])
    else:
        trees.append([relationship])
    return trees
