"""Create something similar to the SnomedCT monolith edition from any
source SnomedCT editions. This will take one or more editions and merge
them for unique entries.

Uniqueness varies slightly by table, but in all cases to does not include
effective time. Of duplicated entries, the one with the latest effective time
is taken forward.
"""
from snomed_ct import (
    __version__,
    __name__ as pkg_name,
    common as common,
    build
)
from pyaddons.flat_files import header
from sqlalchemy_config import config as cfg
from pyaddons import log
from tqdm import tqdm
from merge_sort import chunks, merge
from datetime import datetime
import shutil
import argparse
import re
import os
import sys
import json
import csv
import tempfile
# import pprint as pp


_PROG_NAME = 'snomed-ct-munge'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`).
"""
DELIMITER = "\t"
"""The delimiter of the token file (`str`).
"""


csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``snmed_ct.build.build_snomed_ct``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    db_url = None
    if not os.path.isdir(args.indirs[-1]):
        db_url = args.indirs.pop(-1)

    indirs = [i for i in args.indirs if os.path.isdir(i)]

    # Get a sessionmaker to create sessions to interact with the database
    sm = cfg.get_sessionmaker(
        args.config_section, common._DEFAULT_PREFIX, url_arg=db_url,
        config_arg=args.config, config_env=None,
        config_default=common._DEFAULT_CONFIG
    )

    # Get the session to query/load
    session = sm()

    try:
        # Make sure the database exists (or does not exist)
        cfg.create_db(sm, exists=args.exists)

        # Build the SnomedCT release
        munge_snomed_ct(
            session, indirs, args.outdir, verbose=args.verbose,
            tmpdir=args.tmp
        )
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'indirs',
        type=str, nargs="+",
        help="The input directories from the zip archive."
    )
    parser.add_argument(
        'db_url',
        nargs='?',
        type=str,
        help="The database connection parameters. This can be"
        " either an SQLAlchemy connection URL or filename if using SQLite. If"
        " you do not want to put full connection parameters on the cmd-line "
        "use the config file to supply the parameters"
    )
    parser.add_argument(
        '-o', '--outdir',
        type=str, default=".",
        help="The output root directory, the munged data release will be "
        "output here. This defaults to the PWD"
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/{0}".format(
            os.path.basename(common._DEFAULT_CONFIG)
        ),
        help="The location of the config file"
    )
    parser.add_argument(
        '-s', '--config-section',
        type=str,
        default=common._DEFAULT_SECTION,
        help="The section name in the config file"
    )
    parser.add_argument(
        '-T', '--tmp',
        type=str,
        help="The location of tmp, if not provided will use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Log output to STDERR, use -vv for progress monitors"
    )
    parser.add_argument(
        '-e', '--exists',  action="store_true",
        help="An empty database already exists, so will not be created."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()

    # Because the default is ~/ for Sphinx docs
    args.config = os.path.realpath(os.path.expanduser(args.config))
    args.outdir = os.path.realpath(os.path.expanduser(args.outdir))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_snomed_ct(session, indirs, outdir, verbose=False, tmpdir=None):
    """Add a DM+D database version to the prescription database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The session object attached to the snimed CT database.
    indirs : `list` of `str`
        The source root directory for various editions that need to be merged.
    outdir : `str`
        The output root directory, the release directory will be created within
        this.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use for some sorting of files, if not provided the
        default system temp location is used.
    """
    # prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    logger.info("reading release info...")
    releases = []
    for i in indirs:
        logger.info(f"using indir: {i}")
        release_info = common.extract_dir_info(i)

        use_release = common.DELTA_RELEASE
        if common.FULL_RELEASE in release_info.release:
            use_release = common.FULL_RELEASE
        elif common.SNAPSHOT_RELEASE in release_info.release:
            use_release = common.SNAPSHOT_RELEASE

        releases.append((use_release, release_info))

    build_dir = tempfile.mkdtemp(dir=tmpdir, prefix="munge_")
    try:
        munge_concepts(releases, build_dir, tmpdir=tmpdir, verbose=verbose)
        munge_descriptions(releases, build_dir, tmpdir=tmpdir, verbose=verbose)
        munge_text_def(releases, build_dir, tmpdir=tmpdir, verbose=verbose)
        munge_relationships(releases, build_dir, tmpdir=tmpdir,
                            verbose=verbose)
        munge_stated_relationships(releases, build_dir, tmpdir=tmpdir,
                                   verbose=verbose)
        munge_simple_refsets(releases, build_dir, tmpdir=tmpdir,
                             verbose=verbose)

        # Now we want to create a release info file, so all the files can be
        # built with the build pipeline
        create_release_info_file(releases, build_dir)
    except Exception:
        shutil.rmtree(build_dir)
        raise

    outdir = os.path.realpath(os.path.expanduser(outdir))
    logger.info(f"moving to outdir: {outdir}...")

    build_location = move_build(build_dir, outdir)
    logger.info(f"build location: {build_location}")

    # Now build the munged release
    build.build_snomed_ct(session, build_location, verbose=verbose,
                          tmpdir=tmpdir, release=common.MUNGE_RELEASE)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def move_build(build_dir, outdir):
    """Move the build directory to the final location.

    Parameters
    ----------
    build_dir : `str`
        The path to the build directory.
    outdir : `str`
        The root of the output directory where the build will be moved to.

    Returns
    -------
    final_build_dir : `str`
        The full path to the final build directory.
    """
    effective_time = datetime.now().strftime(common.DATE_FORMAT)
    out_basename = (
        f"SnomedCT_{common.MUNGE_RELEASE}"
        f"RF2_ALPHA_{effective_time}0000000Z"
    )
    outpath = os.path.join(outdir, out_basename)
    shutil.move(build_dir, outpath)
    return outpath


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_release_info_file(releases, root_dir):
    """Build a release info file for the munge edition based on all of the
    input release files.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    """
    outpath = os.path.join(root_dir, common.RELEASE_INFO_FILE)
    effective_time = datetime.now().strftime(common.DATE_FORMAT)

    modules = set()
    lang_refsets = set()
    for ur, ri in releases:
        lang_refsets.update(ri.lang_refsets)
        modules.update(ri.modules)

    munge_release = dict(
        effectiveTime=effective_time,
        modules=dict((i, "not_specified") for i in modules),
        languageRefsets=[
            dict(id=i, term="not_specified") for i in lang_refsets
        ]
    )

    with open(outpath, 'wt') as f:
        json.dump(munge_release, f)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_concepts(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the concept data into a single unique concepts file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `definitionStatusId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    # Descriptions carry textual info
    is_text = False
    munge_type = 'concepts'
    file_template = common.CONCEPT_FILE
    read_func = common.read_sct_concept_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', int),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('definitionStatusId', int)
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_descriptions(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the description data into a single unique description file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `conceptId`, `typeId`, `term`, `caseSignificanceId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    # Descriptions carry textual info
    is_text = True
    munge_type = 'descriptions'
    file_template = common.DESCRIPTION_FILE
    read_func = common.read_sct_desc_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', int),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('conceptId', int),
        ('typeId', int),
        ('term', str),
        # ('caseSignificanceId', int)
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_text_def(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the text definition data into a single unique text definition file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `conceptId`, `typeId`, `term`, `caseSignificanceId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    # Descriptions carry textual info
    is_text = True
    munge_type = 'text definitions'
    file_template = common.TEXT_DEFINITION_FILE
    read_func = common.read_sct_text_def_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', int),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('conceptId', int),
        ('typeId', int),
        ('term', str),
        ('caseSignificanceId', int)
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_relationships(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the relationship data into a single unique relationships file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `sourceId`, `destinationId`, `typeId`, `characteristicTypeId`,
    `modifierId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    is_text = False
    munge_type = 'relationships'
    file_template = common.RELATIONSHIP_FILE
    read_func = common.read_sct_rel_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', int),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('sourceId', int),
        ('destinationId', int),
        # ('relationshipGroup', int),
        ('typeId', int),
        ('characteristicTypeId', int),
        ('modifierId', int),
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose, stated_rel=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_stated_relationships(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the relationship data into a single unique relationships file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `sourceId`, `destinationId`, `typeId`, `characteristicTypeId`,
    `modifierId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    is_text = False
    munge_type = 'stated relationships'
    file_template = common.STATED_RELATIONSHIP_FILE
    read_func = common.read_sct_rel_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', int),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('sourceId', int),
        ('destinationId', int),
        # ('relationshipGroup', int),
        ('typeId', int),
        ('characteristicTypeId', int),
        ('modifierId', int),
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose, stated_rel=True)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def munge_simple_refsets(releases, root_dir, tmpdir=None, verbose=False):
    """Munge the relationship data into a single unique relationships file.

    Uniqueness is based on `id`, `effectiveTime`, `moduleId`,
    `sourceId`, `destinationId`, `typeId`, `characteristicTypeId`,
    `modifierId`.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    """
    is_text = False
    munge_type = 'simple refsets'
    file_template = common.SIMPLE_REFSET_FILE
    read_func = common.read_der_simple_refset_file

    # The keys to sort on and determine uniqueness
    keys = [
        ('id', str),
        # ('effectiveTime', int),
        ('moduleId', int),
        ('refsetId', int),
        ('referencedComponentId', int),
    ]
    _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=is_text, munge_type=munge_type, tmpdir=tmpdir,
                   verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _munge_to_file(releases, root_dir, read_func, file_template, keys,
                   is_text=False, munge_type="data", tmpdir=None,
                   verbose=False, **kwargs):
    """A generalised munge function. To create uniueq entries in an output
    file.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    root_dir : `str`
        The path to the root directory for the munge build.
    read_func : `function`
        The function to read the input files with.
    file_template : `str`
        A template used to identify the input files.
    keys : `list` of `tuple`
        Key column and their datatypes that will form a unique entry and be
        used for the external merge sort. Each type has the column name at
        ``[0]`` and the data type i.e. ``int`` at ``[1]``.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.
    munge_type : `str`, optional, default: `data`
        A string to enter into the log messages, to indicate what is being
        munged.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    **kwargs
        Additional keyword arguments passed through to the ``read_func``.
    """
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    logger.info(f"munging {munge_type} #releases: {len(releases)}")

    # The csv keyword arguments
    csv_kwargs = dict(delimiter=DELIMITER)

    # Get the output path to the file
    outpath = create_file_path(
        root_dir, file_template, is_text=is_text
    )

    # Make sure the output directories have been created
    create_terminology_dir(root_dir)
    create_refseq_content_dir(root_dir)

    key_func = get_key_func(keys)

    infiles = get_input_files(
        releases, file_template, is_text=is_text
    )
    header_row, dialect, has_header, skiplines = header.detect_header(
        infiles[0]
    )

    nunique = 0
    with open(outpath, 'wt') as outcsv:
        writer = csv.DictWriter(outcsv, header_row, **csv_kwargs)
        writer.writeheader()
        for row in _munge_releases(
                releases,
                read_func,
                key_func,
                is_text=is_text,
                tmpdir=tmpdir,
                verbose=verbose,
                munge_type=munge_type,
                csv_kwargs=csv_kwargs,
                header_row=header_row,
                **kwargs
        ):
            writer.writerow(row)
            nunique += 1
    logger.info(f"found # unique {munge_type}: {nunique}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _munge_releases(releases, read_func, key, is_text=False,
                    tmpdir=None, verbose=False, munge_type="data",
                    csv_kwargs=dict(), header_row=[], **kwargs):
    """A generalised file merging function that will yield unique rows based
    on keys.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    read_func : `function`
        The function to read the input files with.
    key : `function`
        The key function used to sort/determine unique-ness.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.
    tmpdir : `str`, optional, default: `NoneType`
        The temp directory to use for external merge sorting. If not provided
        then the default system temp location is used.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress and if > 1 progress monitors.
    munge_type : `str`, optional, default: `data`
        A string to enter into the log messages, to indicate what is being
        munged.
    csv_kwargs : `dict`
        Keyword arguments to be passed through to Python csv.
    header_row : `list` of `str`
        The header row of the files being munged.
    **kwargs
        Additional keyword arguments passed through to the ``read_func``.

    Yields
    ------
    unique_row : `dict`
        A unique row (based on the key function). dict keys are column names
        values are column values.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)

    chunk_dir = tempfile.mkdtemp(dir=tmpdir, prefix=f"{munge_type}_munge_")
    total = 0
    try:
        with chunks.CsvDictExtendedChunks(
                chunk_dir, key, header=header_row, **csv_kwargs
        ) as chunker:
            for use_release, release_info in releases:
                logger.info(f"processing {munge_type}: {release_info.path}")
                for row in read_func(release_info.path, release_info.date,
                                     release=use_release, verbose=prog_verbose,
                                     active_bool=False, int_date=True,
                                     **kwargs):
                    chunker.add_row(row)
                    total += 1

        kwargs = dict(
            disable=not verbose,
            leave=False,
            unit=" rows",
            total=total,
            desc=f"[info] extracting unique {munge_type}"
        )

        with merge.CsvDictIterativeHeapqMerge(
                chunker.chunk_file_list, key=key, tmpdir=chunk_dir,
                csv_kwargs=csv_kwargs
        ) as merger:
            row_buffer = []
            prev_keys = ()
            test_date = True
            for row in tqdm(merger, **kwargs):
                keys = key(row)
                if keys == prev_keys:
                    row_buffer.append(row)
                else:
                    if test_date is True:
                        try:
                            row_buffer.sort(
                                key=lambda x: x['effectiveTime'],
                                reverse=True
                            )
                        except KeyError:
                            test_date = False
                    try:
                        yield row_buffer[0]
                    except IndexError:
                        # First row
                        pass
                    row_buffer = [row]
                prev_keys = keys
    finally:
        shutil.rmtree(chunk_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_key_func(keys):
    """Generate a function from the keys that will return row keys for sorting
    and determining unique rows.

    Parameters
    ----------
    keys : `list` of `tuple`
        Key column and their datatypes that will form a unique entry and be
        used for the external merge sort. Each type has the column name at
        ``[0]`` and the data type i.e. ``int`` at ``[1]``.

    Returns
    -------
    key_func : `function`
        The key function.
    """
    def _key_func(x):
        return [p(x[i]) for i, p in keys]
    return _key_func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_input_files(releases, file_template, is_text=False):
    """Get the input files from a release, that correspond to the file
    template.

    Parameters
    ----------
    releases : `list` of `tuple`
        The releases to process from, each tuple has the required release to
        extract from and the release info object.
    file_template : `str`
        A template used to identify the input files.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.

    Returns
    -------
    input_files : `list` of `str`
        The input files paths extracted from the releases.
    """
    infiles = []
    for use_release, release_info in releases:
        infiles.append(
            common.get_file(
                release_info.path, file_template, release_info.date,
                release=use_release, is_text=is_text
            )
        )
    return infiles


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_terminology_dir(root_dir):
    """Ensure the terminology output directory is in place.

    Parameters
    ----------
    root_dir : `str`
        The path to the root directory for the munge build.

    Returns
    -------
    terminology_dir : `str`
        The full path to the terminology directory.
    """
    term_release = common.TERMINOLOGY_RELATIVE_PATH.format(
        release=common.MUNGE_RELEASE
    )
    term_dir = os.path.join(root_dir, term_release)

    os.makedirs(term_dir, exist_ok=True)
    return term_dir


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_refseq_content_dir(root_dir):
    """Ensure the ``./Refset/Content`` output directory is in place.

    Parameters
    ----------
    root_dir : `str`
        The path to the root directory for the munge build.

    Returns
    -------
    terminology_dir : `str`
        The full path to the content directory.
    """
    release_dir = common.REFSET_CONTENT_RELATIVE_PATH.format(
        release=common.MUNGE_RELEASE
    )
    full_dir = os.path.join(root_dir, release_dir)

    os.makedirs(full_dir, exist_ok=True)
    return full_dir


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_file_path(root_path, file_template, is_text=False):
    """Create an munge build output file path based on the root_path to
    the build directory  and a file template.

    Parameters
    ----------
    root_path : `str`
        The path to the root directory for the munge build.
    file_template : `str`
        A template used to identify the input files.
    is_text : `bool`, optional, default: `False`
        Does the file contain textual information, i.e term descriptions. This
        is used to adjust the file name.

    Returns
    -------
    full_path : `str`
        The full path the file defined by ``root_path`` and ``file_template``.
    """
    text = ""
    if is_text is True:
        text = "-en"

    file_basename = file_template.format(
        release=common.MUNGE_RELEASE,
        text=text,
        date=datetime.now().strftime(common.DATE_FORMAT)
    )

    file_basename = re.sub(
        re.escape(r'*{0}'.format(common.MUNGE_RELEASE)),
        'ALL{0}'.format(common.MUNGE_RELEASE),
        file_basename
    )
    file_basename = re.sub(re.escape(r'[GI][BN]*'), 'GB', file_basename)
    return os.path.join(root_path, file_basename)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
