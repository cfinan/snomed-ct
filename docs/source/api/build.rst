``snomed_ct`` API
=================

Currently, other than the ORM the only real API is a direct call to the build function. Nevertheless, I have also implemented readers for all the files in the ``snomed_ct.common`` module that may be of use.

``snomed_ct.build`` module
--------------------------

.. autofunction:: snomed_ct.build.build_snomed_ct

.. include:: ../data_dict/snomed-ct-orm.rst

``snomed_ct.common`` module
---------------------------

.. automodule:: snomed_ct.common
   :members:
   :undoc-members:
   :show-inheritance:

``snomed_ct.munge`` module
--------------------------

.. autofunction:: snomed_ct.munge.munge_snomed_ct
